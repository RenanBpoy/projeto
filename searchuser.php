<?php include('header.php');
include('leftsidebar.php');
include('rightsidebar.php');

if(isset($_GET['pesquisar_usuario'])){
  $nome = $_GET['pesquisar_usuario'];
}
else{
  $nome = '';
}

$usuario = new Usuario();
$usuario->setNome($nome);

$array = $usuario->buscarUsuario();

?>

<div id="wrapper" class="mt-5">

<?php

while($usuario = mysqli_fetch_assoc($array)): 
?>



    <div class="col-12 col-sm-6 col-md-4 col-lg-4">
    <?php echo '<a href="userprofile.php?id='.$usuario['id'].'">' ?>
      <div class="our-team">
        <div class="picture">
          <img class="img-fluid" <?php echo 'src="images/imagem_usuario/'.$usuario['imagem'].'"' ?>>
        </div>
        <div class="team-content">
          <h3 class="name"><?php echo $usuario['nome']; echo '<br>'; echo $usuario['sobrenome'] ?></h3>
          <h4 class="title"><?php echo $usuario['username'] ?></h4>
          
              <?php 
                if ($usuario['id'] == $_SESSION['usuario']['id']){ ?>
                <a href="userprofile.php" class="btn btn-primary button-profile mt-5">Ver perfil</a>
                <?php 
                }
                else{ 
                  $seguir = new Seguir();
                  $seguir->setIdUsuario1($_SESSION['usuario']['id']);
                  $seguir->setIdUsuario2($usuario['id']);
                  $array_amigos = $seguir->consultarSeguir();
                  $amigos = mysqli_fetch_array($array_amigos);
                  if($amigos){?>

                    
                    <a <?php echo 'href="deixarseguir.php?id_usuario2='.$usuario["id"].'"' ?> class="btn btn-danger button-profile mt-5">Deixar de Seguir</a>
                  

                <?php
                  }
                  else{
                ?>
                  <a <?php echo 'href="seguir.php?id_usuario2='.$usuario["id"].'"' ?> class="btn btn-success button-profile mt-5">Seguir</a>
                  <?php
                  }
                }
            ?>

        </div>
      </div>
    </div>
    </a>




<?php endwhile ?>


</div>

<?php include('footer.php') ?>