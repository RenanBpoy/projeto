<?php include('header.php');
include('leftsidebar.php');
include('rightsidebar.php');

$nome_popular = $_GET['pesquisar_catalogacao'];
$catalogacao = new Catalogacao();
$catalogacao->setNomePopular($nome_popular);

 ?>
        

    <div id="wrapper">


        <section class="section lb">
            <div class="container">
                <div class="section-title text-center">
                    <h3>Resultados da pesquisa</h3>
                </div><!-- end title -->

            	<div class="row">
                    <div class="col-md-12">
                        <div class="portfolio row with-desc">

                            <?php 


                            $array = $catalogacao->buscarCatalogacao();
                            while($catalogacao = mysqli_fetch_assoc($array)): 
                            $timeStamp = $catalogacao['data_hora_catalogacao'];
                            $timeStamp = date( "d/m/Y", strtotime($timeStamp));

                                ?>

                			<?php include('modelo_catalogacao.php') ?>

                		    <?php endwhile ?>

                        </div>
                    </div>
                </div>

            </div><!-- end container -->
        </section><!-- end section -->

        

<?php include('footer.php') ?>