<?php include('header.php');
include('leftsidebar.php');
include('rightsidebar.php');

$usuario = new Usuario();

$novo_nome = md5(time()).'.jpg';
$_SESSION['foto_usuario'] = $novo_nome;

if(isset($_GET['id']))
  {$_SESSION['id_user'] = $_GET['id'];
}
else{
  $_SESSION['id_user'] = $_SESSION['usuario']['id'];
}




$linhas = mysqli_query($con, 'SELECT * from usuario where id="'.$_SESSION['id_user'].'"');
while($usuario = mysqli_fetch_assoc($linhas)):


    $data_nascimento =$usuario['data_nascimento'];
    $idade = (date('Y') - date('Y',strtotime($data_nascimento)));




 ?>




    <div id="wrapper">

        <form action="salvar_perfil.php" method="POST">

        <section class="section lb">
            <div class="container">
            	<div class="row">
            		<div class="col-4">

                  <img width="200" height="200" class="circle-image picture"  <?php echo ' src="images/imagem_usuario/'.$usuario['imagem'].'" ' ?>/>

                  
            		</div>
            		<div class="col-4 mt-5">
            			<h1 class="h0"><?php echo $usuario['nome'] ?></h1>
            			<h1>@<?php echo $usuario['username'] ?></h1>
            		</div>
            		<div class="col-4 mt-5">
                <?php 
                if ($_SESSION['id_user'] == $_SESSION['usuario']['id']){ ?>
                <a href="editarperfil.php" class="btn btn-primary button-profile mt-5">Editar perfil</a>
                <?php 
                }
                else{ 
                  $seguir = new Seguir();
                  $seguir->setIdUsuario1($_SESSION['usuario']['id']);
                  $seguir->setIdUsuario2($_SESSION['id_user']);
                  $array = $seguir->consultarSeguir();
                  $amigos = mysqli_fetch_array($array);
                  if($amigos){?>

                    
                    <a <?php echo 'href="deixarseguir.php?id_usuario2='.$_SESSION['id_user'].'"' ?> class="btn btn-danger button-profile mt-5">Deixar de Seguir</a>
                  

                <?php
                  }
                  else{
                ?>
                  <a <?php echo 'href="seguir.php?id_usuario2='.$_SESSION['id_user'].'"' ?> class="btn btn-success button-profile mt-5">Seguir</a>
                  <?php
                  }
                }
                ?>
            			
            		</div>
            	</div>


                <div class="section-title text-center primary-menu">

              <center><nav class="navbar navbar-expand-lg navbar-light">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#conteudoNavbarSuportado" aria-controls="conteudoNavbarSuportado" aria-expanded="false" aria-label="Alterna navegação">
                  <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="conteudoNavbarSuportado">
                  <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                      <a class="nav-link" <?php echo 'href="userprofile.php?id='.$_SESSION['id_user'].'"' ?>>Publicações <span class="sr-only">(página atual)</span></a>
                    </li>
                    <li class="nav-item active">
                      <a class="nav-link" <?php echo 'href="userprofileinformacoes.php?id='.$_SESSION['id_user'].'"' ?>>Perfil <span class="sr-only">(página atual)</span></a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" <?php echo 'href="userprofilecatalogacoes.php?id='.$_SESSION['id_user'].'"' ?>>Catalogações</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" <?php echo 'href="userprofilepremiacoes.php?id='.$_SESSION['id_user'].'"' ?>>Premiações</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" <?php echo 'href="userprofileseguidores.php?id='.$_SESSION['id_user'].'"' ?>>Seguidores</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" <?php echo 'href="userprofileseguindo.php?id='.$_SESSION['id_user'].'"' ?>>Seguindo</a>
                    </li>
                  </ul>
                </div>
              </nav></center>
                    <hr>
                    <h3>Perfil</h3>
                    <hr>



                </div><!-- end title -->

  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputEmail4">Nome</label>
      <input name="nome" type="text" class="form-control" id="inputEmail4" placeholder="Nome" required <?php echo 'value="'.$usuario['nome'].'"' ?> disabled>
    </div>
    <div class="form-group col-md-6">
      <label for="inputPassword4">Sobrenome</label>
      <input name="sobrenome" type="text" class="form-control" id="inputPassword4" placeholder="Sobrenome" required <?php echo 'value="'.$usuario['sobrenome'].'"' ?> disabled>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputEmail4">Email</label>
      <input name="email" type="email" class="form-control" id="inputEmail4" placeholder="Email" required <?php echo 'value="'.$usuario['email'].'"' ?> disabled>
    </div>
    <div class="form-group col-md-6">
      <label for="inputCity">Username</label>
      <input name="username" type="text" class="form-control" id="inputCity" <?php echo 'value="@'.$usuario['username'].'"' ?> disabled>
    </div>
  </div>
  <div class="form-row">

    <div class="form-group col-md-4">
      <label for="inputState">Sexo</label>
      <select id="inputState" class="form-control" name="id_sexo" disabled="disabled">
        <option value="" disabled="disabled">Sexo</option>
        <?php $linhas = mysqli_query($con, 'SELECT * from sexo');
        while($sexo = mysqli_fetch_assoc($linhas)): ?>

          <option <?php if($sexo['id'] == $usuario['id_sexo']){echo 'selected="selected"';} echo 'value="'.$sexo['id'].'"' ?>><?php echo $sexo['descricao'] ?></option>
        <?php endwhile ?>
      </select>
    </div>
    <div class="form-group col-md-4">
      <label for="inputZip">Idade</label>
      <input name="crbio" type="text" class="form-control" id="inputZip" <?php echo 'value="'.$idade.' anos"' ?> disabled>
    </div>
    <div class="form-group col-md-4">
      <label for="inputZip">CRBio</label>
      <input name="crbio" type="text" class="form-control" id="inputZip" <?php echo 'value="'.$usuario['crbio'].'"' ?> disabled>
    </div>
  </div>


<?php
endwhile; ?>




<?php include('footer.php');
?>