<?php 
include('header.php');
include('leftsidebar.php');
include('rightsidebar.php');
?>

<?php 

  $novo_nome = md5(time()).'.jpg';
  $_SESSION['foto'] = $novo_nome;



?>

<div id="wrapper">
<center><h1>Registrar Catalogação</h1></center>
<hr noshade >
<form method="post" action="salvar_catalogacao.php" enctype="multipart/form-data">
<div class="row mt-2 mb-5 mx-3">
	<div class="col-6 mt-5">
    <label>Nome Popular</label>
		<input class="form-control" type="text" name="nome_popular" placeholder="Ser humano"  required>
    <label class="mt-4">Filo</label>
		<input class="form-control " type="text" name="filo" placeholder="Chordata"  required>
    <label class="mt-4">Ordem</label>
		<input class="form-control" type="text" name="ordem" placeholder="Primates"  required>
    <label class="mt-4">Familia</label>
		<input class="form-control" type="text" name="familia" placeholder="Hominidae"  required>
    <label class="mt-4">Genero</label>
		<input class="form-control" type="text" name="genero" placeholder=" Homo"  required>
    <label class="mt-4">Espécie</label>
		<input class="form-control" type="text" name="especie" placeholder=" Homo sapiens"  required>
    </div>
    <div class="col-6 mt-5">
      <div class="row mt-5">
        <div class="col">
        <select id="inputState" class="form-control" name="reino" required>
                  <option value='' disabled="disabled" selected="selected">Reino</option>
                <?php $linhas = mysqli_query($con, 'SELECT * from reino');
                while($reino = mysqli_fetch_assoc($linhas)): ?>
                  <option <?php echo 'value="'.$reino['id'].'"' ?>><?php echo $reino['descricao'] ?></option>
                <?php endwhile ?>
        </select>
		</div>
    </div>
        <label class="mt-4">Classe</label>
		<input class="form-control" type="text" name="classe" placeholder="Mammalia"  required>
      <div class="form-group">

        <label class="mt-4">Descrição</label>
        <textarea class="form-control" rows="5" name="descricao" id="comment"  placeholder="Descrição"></textarea>
        <label class="mt-4">Observação</label>
        <textarea class="form-control" rows="5" name="observacao" id="comment" placeholder="Observação"></textarea>
      </div>

    </div>
    
  </div>
  <div class="row">
      <div class="col">
      <center>
            <input type="file" name="upload_image" id="upload_image" />
            <br />
            <div class="mt-3" id="uploaded_image"></div>
      </center>
      </div>
      </div>
  <div class="col-6 mt-5">

  </div>

<div id="uploadimageModal" class="modal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal">&times;</button>
      		</div>
      		<div class="modal-body">
        		<div class="row">
  					<div class="col-md-8 text-center">
						  <div id="image_demo" style="width:350px; margin-top:30px"></div>
  					</div>
  					<div class="col-md-4" style="padding-top:30px;">
  						<br />
  						<br />
  						<br/>

					</div>
				</div>
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-success crop_image" >Salvar</button>
      		</div>
    	</div>
    </div>
</div>

<script>  
$(document).ready(function(){

	$image_crop = $('#image_demo').croppie({
    enableExif: true,
    viewport: {
      width:300,
      height:200,
      type:'square' //circle
    },
    boundary:{
      width:300,
      height:300
    }
  });

  $('#upload_image').on('change', function(){
    var reader = new FileReader();
    reader.onload = function (event) {
      $image_crop.croppie('bind', {
        url: event.target.result
      }).then(function(){
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
    $('#uploadimageModal').modal('show');
  });

  $('.crop_image').click(function(event){
    $image_crop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function(response){
      $.ajax({
        url:"upload.php",
        type: "POST",
        data:{"image": response},
        success:function(data)
        {
          $('#uploadimageModal').modal('hide');
          $('#uploaded_image').html(data);
        }
      });
    })
  });

});  
</script>





		</div>
	</div>
	<div class="col mt-5">
		<div class="row">
			<div class="col-md-12">
				<center><img class="img-fluid" id="output"/></center>
			</div>
		</div>
	</div>
</div>
<center><button type="submit" class="btn btn-primary mb-5">Catalogar</button></center>
</form>
</div>
<?php include('footer.php') ?>

