        <header class="vertical-left-header">
            <div class="vertical-left-header-wrapper">
                <nav class="nav-menu">
                    <div class="row mt-4 ml-3">
                        <div class="col-4 mt-4">
                            <a <?php echo 'href="userprofile.php?id='.$_SESSION['usuario']['id'].'"' ?>><img class="circle-image zoom" <?php echo ' src="images/imagem_usuario/'.$_SESSION['usuario']['imagem'].'" ' ?> alt="" width="60" height="60"></a>
                        </div>
                        <div class="col-8 mt-4 ml-negative">
                            <h3 class="mt-2 text-default"><?php echo $_SESSION['usuario']['nome'] ?></h3>
                            <h5 class="mt-1 text-default">@<?php echo $_SESSION['usuario']['username'] ?></h5>
                        </div>
                    </div><!-- end logo -->



                    <ul class="primary-menu">
                        <li class="child-menu"><a href="home.php">Página inicial</a></li>
                        <li class="child-menu"><a href="chart.php">Informações</a></li>
                        <li class="child-menu"><a href="catalogacao.php">Registrar Catalogação</a>
                        </li>
                        <li class="child-menu"><a href="minhas_catalogacoes.php">Minhas Catalogações<i class="fa fa-angle-right"></i></a>
                            <div class="sub-menu-wrapper">
                                <ul class="sub-menu center-content">
                                <?php $linhas = mysqli_query($con, 'SELECT * from reino');
                                while($reino = mysqli_fetch_assoc($linhas)): ?>
                                    <li><a <?php echo 'href="minhas_catalogacoes.php?id_reino='.$reino['id'].'"' ?>><?php echo $reino['descricao'] ?></a></li>
                                <?php
                                endwhile;
                                ?>
                                </ul>
                            </div>
                        </li>
                        <li class="child-menu"><a href="premiacao.php">Meus prêmios</a></li>
                        <li class="child-menu"><a href="sair.php">Sair</a></li>
                    
                    </ul>
                    
                    <div class="margin-block"></div>

                    <div class="menu-search">
                        <form action="search.php">
                            <div class="form-group">
                                <input name="pesquisar_catalogacao" type="text" class="form-control" placeholder="Pesquisar catalogação">
                                <button class="btn btn-primary mt-2"><i class="fa fa-search"></i></button>
                            </div>
                        </form>
                    </div><!-- end menu-search -->

                    <div class="margin-block"></div>

                </nav><!-- end nav-menu -->
            </div><!-- end vertical-left-header-wrapper -->
        </header><!-- end header -->