<?php include('header.php');
include('leftsidebar.php');
include('rightsidebar.php'); ?>

<div id="wrapper">

<div id="frame" class="px-5 py-5">

  <div class="content">
    <div class="contact-profile">
      <img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" />
      <p>Harvey Specter</p>
      <div class="social-media">
        <i class="fa fa-facebook" aria-hidden="true"></i>
        <i class="fa fa-twitter" aria-hidden="true"></i>
         <i class="fa fa-instagram" aria-hidden="true"></i>
      </div>
    </div>
    <div class="messages">
      <ul>
        <li class="sent">
          <img src="http://emilcarlsson.se/assets/mikeross.png" alt="" />
          <p>How the hell am I supposed to get a jury to believe you when I am not even sure that I do?!</p>
        </li>
        <li class="replies">
          <img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" />
          <p>When you're backed against the wall, break the god damn thing down.</p>
        </li>
      </ul>
    </div>
    <div class="message-input">
      <div class="wrap">
      <input type="text" placeholder="Write your message..." />
      <i class="fa fa-paperclip attachment" aria-hidden="true"></i>
      <button class="submit"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
      </div>
    </div>
  </div>
</div>
        

<?php include('footer.php') ?>