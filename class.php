<?php

	class Compartilhamento {
		var $id;
		var $id_catalogacao;
		var $id_usuario;
		var $mensagem;

		function getId(){
			return $this->id;
		}

		function setId($id){
			$this->id = $id;
		}

		function getIdCatalogacao(){
			return $this->id_catalogacao;
		}

		function setIdCatalogacao($id_catalogacao){
			$this->id_catalogacao = $id_catalogacao;
		}

		function getIdUsuario(){
			return $this->id_usuario;
		}

		function setIdUsuario($id_usuario){
			$this->id_usuario = $id_usuario;
		}

		function getMensagem(){
			return $this->mensagem;
		}

		function setMensagem($mensagem){
			$this->mensagem = $mensagem;
		}

		function compartilharCatalogacao(){

			$id_usuario = $this->id_usuario;
			$id_catalogacao = $this->id_catalogacao;
			$mensagem = $this->mensagem;
			$banco = $_SESSION['con'];
			if(!$banco){
				die('nao foi possivel conectar:'. mysqli_error());

			}

			$sql = "INSERT INTO `compartilhamento`(`id_usuario`, `id_catalogacao`, `mensagem`) VALUES ($id_usuario, $id_catalogacao, '$mensagem')";

			$executa = mysqli_query($banco, $sql);

		}

		function verCompartilhamento(){

			$id_usuario = $this->id_usuario;
			$id_catalogacao = $this->id_catalogacao;
			$mensagem = $this->mensagem;
			$banco = $_SESSION['con'];
			if(!$banco){
				die('nao foi possivel conectar:'. mysqli_error());

			}

			$sql = 'SELECT ct.id as idcatalogacao, c.id as idcompartilhamento, u.id as id_usuario, r.descricao as descricao_reino, c.*, ct.*, r.*, u.* from compartilhamento c join usuario u on c.id_usuario = u.id join catalogacao ct on c.id_catalogacao = ct.id join reino r on ct.id_reino = r.id ORDER BY c.data_hora_compartilhamento DESC';


			$executa = mysqli_query($banco, $sql);

			return $executa;

		}

		function buscarCompartilhamento(){
			$id = $this->id;

			$banco = $_SESSION['con'];
			if(!$banco){
				die('nao foi possivel conectar:'. mysqli_error());

			}

			$sql = 'SELECT ct.id as idcatalogacao, c.id as idcompartilhamento, r.descricao as descricao_reino, c.*, ct.*, r.*, u.*, s.* from compartilhamento c join usuario u on c.id_usuario = u.id join catalogacao ct on c.id_catalogacao = ct.id join reino r on ct.id_reino = r.id join seguir s on s.id_usuario2 = u.id where c.id = '.$id;

			$executa = mysqli_query($banco, $sql);

			return $executa;

		}

		function editarCompartilhamento(){
			$id = $this->id;
			$mensagem = $this->mensagem;

			$banco = $_SESSION['con'];
			if(!$banco){
				die('nao foi possivel conectar:'. mysqli_error());

			}

			$sql = "UPDATE compartilhamento SET mensagem = '$mensagem' WHERE id = ".$id;

			$executa = mysqli_query($banco, $sql);


		}

		function excluirCompartilhamento(){
			$id = $this->id;


			$banco = $_SESSION['con'];
			if(!$banco){
				die('nao foi possivel conectar:'. mysqli_error());

			}

			$sql = "DELETE FROM compartilhamento WHERE id = $id";

			$executa = mysqli_query($banco, $sql);


		}




	}

	class Seguir {
		var $id_usuario1;
		var $id_usuario2;

		function getIdUsuario1(){
			return $this->id_usuario1;
		}

		function getIdUsuario2(){
			return $this->id_usuario2;
		}

		function setIdUsuario1($id_usuario1){
			$this->id_usuario1 = $id_usuario1;
		}

		function setIdUsuario2($id_usuario2){
			$this->id_usuario2 = $id_usuario2;
		}

		function seguirUsuario(){
			$id_usuario1 = $this->id_usuario1;
			$id_usuario2 = $this->id_usuario2;
			$banco = $_SESSION['con'];
			if(!$banco){
				die('nao foi possivel conectar:'. mysqli_error());

			}

			$sql = "INSERT INTO `seguir`(`id_usuario1`, `id_usuario2`) VALUES ($id_usuario1, $id_usuario2)";

			$executa = mysqli_query($banco, $sql);

		}

		function consultarSeguir(){
				$id_usuario1 = $this->id_usuario1;
				$id_usuario2 = $this->id_usuario2;
				$banco = $_SESSION['con'];
				if(!$banco){
					die('nao foi possivel conectar:'. mysqli_error());

				}

				$sql = "SELECT * FROM seguir where id_usuario1 = $id_usuario1 and id_usuario2 = $id_usuario2";

				$executa = mysqli_query($banco, $sql);

				return $executa;

		}

		function consultarAmizade(){
				$id_usuario1 = $this->id_usuario1;

				$banco = $_SESSION['con'];
				if(!$banco){
					die('nao foi possivel conectar:'. mysqli_error());

				}

				$sql = "SELECT * FROM seguir s JOIN usuario u on u.id = s.id_usuario2 where id_usuario1 = $id_usuario1 order by u.nome";

				$executa = mysqli_query($banco, $sql);

				return $executa;

		}



		function pararSeguirUsuario(){
			$id_usuario1 = $this->id_usuario1;
			$id_usuario2 = $this->id_usuario2;
			$banco = $_SESSION['con'];
			if(!$banco){
				die('nao foi possivel conectar:'. mysqli_error());

			}

			$sql = "DELETE FROM seguir WHERE id_usuario1 = $id_usuario1 and id_usuario2 = $id_usuario2";

			echo $sql;

			$executa = mysqli_query($banco, $sql);


		}


	}

	class Usuario {
		var $id;
		var $nome;
		var $sobrenome;
		var $username;
		var $email;
		var $senha;
		var $crbio;
		var $data_nascimento;
		var $data_hora_cadastro;
		var $imagem;
		var $id_sexo;


		function getId() {
			return $this->id;
		}

		function getNome(){
			return $this->nome;
		}

		function getSobrenome(){
			return $this->sobrenome;
		}

		function getUsername(){
			return $this->username;
		}

		function getEmail(){
			return $this->codigo;
		}

		function getSenha(){
			return $this->senha;
		}

		function getCRBio(){
			return $this->crbio;
		}

		function getDataNascimento(){
			return $this->data_nascimento;
		}

		function getDataHoraCadastro(){
			return $this->data_hora_cadastro;
		}

		function getImagem(){
			return $this->imagem;
		}

		function getIdSexo(){
			return $this->id_sexo;
		}


		function setId($id){
			$this->id = $id;
		}

		function setNome($nome){
			$this->nome = $nome;
		}

		function setSobrenome($sobrenome){
			$this->sobrenome = $sobrenome;
		}


		function setUsername($username){
			$this->username = $username;
		}

		function setEmail($email){
			$this->email = $email;
		}

		function setSenha($senha){
			$this->senha = $senha;
		}


		function setCRBio($crbio){
			$this->crbio = $crbio;
		}

		function setDataNascimento($data_nascimento){
			$this->data_nascimento = $data_nascimento;
		}

		function setDataHoraCadastro($data_hora_cadastro){
			$this->datahoraCadastro = $data_hora_cadastro;
		}


		function setImagem($imagem){
			$this->imagem = $imagem;
		}

		function setIdSexo($id_sexo){
			$this->id_sexo = $id_sexo;
		}


		function criarCadastro(){
		$nome = $this->nome;
		$sobrenome = $this->sobrenome;
		$username = $this->username;
		$email = $this->email;
		$senha = $this->senha;
		$crbio = $this->crbio;
		$data_nascimento = $this->data_nascimento;
		$id_sexo = $this->id_sexo;
		$banco = $_SESSION['con'];
		if(!$banco){
			die('nao foi possivel conectar:'. mysqli_error());

		 }
		$sql = "INSERT INTO `usuario`(`nome`, `sobrenome`, `crbio`, `username`,`data_nascimento`, `email`, `senha`, `id_sexo`) VALUES ('$nome', '$sobrenome', '$crbio', '$username', '$data_nascimento', '$email', '$senha', $id_sexo)";

		$executa = mysqli_query($banco, $sql);


		}



		function alterarCadastro(){
		$id = $this->id;
		$nome = $this->nome;
		$sobrenome = $this->sobrenome;
		$username = $this->username;
		$email = $this->email;
		$senha = $this->senha;
		$crbio = $this->crbio;
		$data_nascimento = $this->data_nascimento;
		$id_sexo = $this->id_sexo;
		$banco = $_SESSION['con'];
		if(!$banco){
			die('nao foi possivel conectar:'. mysqli_error());

		 }
		$sql = "UPDATE usuario SET nome = '$nome', sobrenome = '$sobrenome', username = '$username', email = '$email', data_nascimento = '$data_nascimento', id_sexo = '$id_sexo', crbio = '$crbio', senha = '$senha' WHERE id = '$id'";

		$sql_att = "SELECT * FROM usuario WHERE id = '$id'";

		$update = mysqli_query($banco, $sql_att);

		$executa = mysqli_query($banco, $sql);

		return $update;

		}

		function excluirCadastro(){
		$id = $this->id;
		$banco = $_SESSION['con'];
		if(!$banco){
			die('nao foi possivel conectar:'. mysqli_error());

		 }
		$sql = 'DELETE FROM usuario WHERE id="'.$id.'"';

		$executa = mysqli_query($banco, $sql);
		session_destroy();

		}

		function fazerLogin(){
		$email = $this->email;
		$senha = $this->senha;
		$banco = $_SESSION['con'];
		if(!$banco){
			die('nao foi possivel conectar:'. mysqli_error());

		 }

		$sql = "SELECT * FROM usuario WHERE (email = '$email' or username = '$email') AND senha = '$senha'";

		$executa = mysqli_query($banco, $sql);

		return $executa;


		}

		function fazerLogoff(){
		session_destroy();
		header("Location:/projeto/index.php");


		}


		function buscarUsuario(){
		$id = $this->id;
		$nome = $this->nome;
		$sobrenome = $this->sobrenome;
		$username = $this->username;
		$email = $this->email;
		$senha = $this->senha;
		$crbio = $this->crbio;
		$data_nascimento = $this->data_nascimento;
		$id_sexo = $this->id_sexo;
		$banco = $_SESSION['con'];
		if(!$banco){
			die('nao foi possivel conectar:'. mysqli_error());

		 }
		$sql = "SELECT * FROM usuario where nome like'%$nome%' or sobrenome like '%$nome%'";

		$executa = mysqli_query($banco, $sql);

		return $executa;

		}




	
	}


	class Catalogacao {

		var $id;
		var $nome_popular;
		var $filo;
		var $classe;
		var $ordem;
		var $familia;
		var $genero;
		var $especie;
		var $data_hora_cadastro;
		var $descricao;
		var $observacao;
		var $imagem_catalogacao;
		var $id_reino;
		var $id_usuario;

		function getId() {
			return $this->id;
		}

		function getNomePopular() {
			return $this->nome_popular;
		}

		function getFilo() {
			return $this->filo;
		}

		function getClasse() {
			return $this->classe;
		}

		function getOrdem() {
			return $this->ordem;
		}

		function getFamilia() {
			return $this->familia;
		}

		function getGenero() {
			return $this->genero;
		}

		function getEspecie() {
			return $this->id;
		}
		
		function getDataHoraCatalogacao() {
			return $this->data_hora_catalogacao;
		}
		
		function getDescricao() {
			return $this->descricao;
		}
		
		function getObservacao() {
			return $this->observacao;
		}
		
		function getImagemCatalogacao() {
			return $this->imagem_catalogacao;
		}
		
		function getIdReino() {
			return $this->id_reino;
		}
		
		function getIdUsuario() {
			return $this->id_usuario;

		}

		function setId($id){
			$this->id = $id;
		}

		function setNomePopular($nome_popular){
			$this->nome_popular = $nome_popular;
		}

		function setFilo($filo){
			$this->filo = $filo;
		}


		function setClasse($classe){
			$this->classe = $classe;
		}

		function setOrdem($ordem){
			$this->ordem = $ordem;
		}

		function setFamilia($familia){
			$this->familia = $familia;
		}


		function setGenero($genero){
			$this->genero = $genero;
		}

		function setEspecie($especie){
			$this->especie = $especie;
		}

		function setDataHoraCatalogacao($data_hora_cadastro){
			$this->data_hora_catalogacao = $data_hora_catalogacao;
		}


		function setDescricao($descricao){
			$this->descricao = $descricao;
		}

		function setObservacao($observacao){
			$this->observacao = $observacao; 
		}
		function setImagemCatalogacao($imagem_catalogacao){
			$this->imagem_catalogacao = $imagem_catalogacao; 
		}
		function setIdReino($id_reino){
			$this->id_reino = $id_reino; 
		}
		function setIdUsuario($id_usuario){
			$this->id_usuario = $id_usuario; 
		}

		function criarCadastro(){
			
			$nome_popular = $this->nome_popular;
			$filo = $this->filo;
			$classe = $this->classe;
			$ordem = $this->ordem;
			$familia = $this->familia;
			$genero = $this->genero;			
			$especie = $this->especie;
			$observacao = $this->observacao;
			$imagem_catalogacao = $this->imagem_catalogacao;
			$descricao = $this->descricao;			
			$data_hora_catalogacao = $this->data_hora_catalogacao;
			$id_usuario = $this->id_usuario;  
			$id_reino = $this->id_reino;
			$banco = $_SESSION['con'];
			if(!$banco){
				die('nao foi possivel conectar:'. mysqli_error());

		 	}
			$sql = "INSERT INTO `catalogacao`(`nome_popular`, `filo`, `classe`,`ordem`, `familia`, `genero`, `especie`, `descricao`, `observacao`,`imagem_catalogacao`, `id_usuario`, `id_reino`) VALUES ('$nome_popular', '$filo', '$classe','$ordem', '$familia', '$genero', '$especie', '$descricao', '$observacao','$imagem_catalogacao', $id_usuario, $id_reino)";
			echo $sql;

			$executa = mysqli_query($banco, $sql);


		}

		function alterarCadastro(){

			$id = $this->id;
			$nome_popular = $this->nome_popular;
			$filo = $this->filo;
			$classe = $this->classe;
			$ordem = $this->ordem;
			$familia = $this->familia;
			$genero = $this->genero;			
			$especie = $this->especie;
			$observacao = $this->observacao;
			$imagem_catalogacao = $this->imagem_catalogacao;
			$descricao = $this->descricao;			
			$data_hora_cadastro = $this->data_hora_cadastro;
			$id_usuario = $this->id_usuario;  
			$id_reino = $this->id_reino;
			$banco = $_SESSION['con'];
			if (!$banco) {
			    die('Não foi possível conectar: ' . mysql_error());
			}

			$sql="UPDATE catalogacao SET nome_popular = '$nome_popular', filo = '$filo', classe = '$classe', ordem = '$ordem', familia = '$familia', genero = '$genero', especie = '$especie', observacao = '$observacao', descricao = '$descricao', id_usuario = '$id_usuario', id_reino = '$id_reino' WHERE id = '$id'";
			$executa = mysqli_query($banco, $sql);

			return $sql;

		}

		function excluirCadastro(){

			$id = $this->id;
			$banco = $_SESSION['con'];
			if(!$banco){
				die('nao foi possivel conectar:'. mysqli_error());
	
			 }
			$sql = 'DELETE FROM catalogacao WHERE id="'.$id.'"';

			$executa = mysqli_query($banco, $sql);

		}

		function compartilharCadastro(){

			$id = $this->id;
			$nome_popular = $this->nome_popular;
			$filo = $this->filo;
			$classe = $this->classe;
			$ordem = $this->ordem;
			$familia = $this->familia;
			$genero = $this->genero;			
			$especie = $this->especie;
			$observacao = $this->observacao;
			$imagem_catalogacao = $this->imagem_catalogacao;
			$descricao = $this->descricao;			
			$data_hora_cadastro = $this->data_hora_cadastro;
			$id_usuario = $this->usuario;  
			$id_reino = $this->reino;

		}


		function buscarCatalogacao(){

			$nome_popular = $this->nome_popular;

			$banco = $_SESSION['con'];
			if (!$banco) {
					die('Não foi possível conectar: ' . mysql_error());
			}

			$sql = 'SELECT r.descricao as descricao_reino, c.id as idcatalogacao, c.*, r.*, u.* from catalogacao c join usuario u on c.id_usuario = u.id join reino r on c.id_reino = r.id  where nome_popular LIKE "%'.$nome_popular.'%" or filo LIKE "%'.$nome_popular.'%" or classe LIKE "%'.$nome_popular.'%" or ordem LIKE "%'.$nome_popular.'%" or familia LIKE "%'.$nome_popular.'%" or genero LIKE "%'.$nome_popular.'%" or especie LIKE "%'.$nome_popular.'%" or r.descricao LIKE "%'.$nome_popular.'%" ORDER BY c.data_hora_catalogacao DESC';

			$executa = mysqli_query($banco, $sql);

			return $executa;

	}

		function buscarCatalogacaoId(){

			$id = $this->id;
			$banco = $_SESSION['con'];
			if (!$banco) {
					die('Não foi possível conectar: ' . mysql_error());
			}

			$sql="SELECT c.descricao as descricao_catalogacao, c.*, r.*, u.* FROM catalogacao c JOIN reino r on c.id_reino = r.id JOIN usuario u on c.id_usuario = u.id WHERE c.id = '$id'";

			$executa = mysqli_query($banco, $sql);

			return $executa;

	}

		function buscarCatalogacaoIdUsuario(){

			$id_usuario = $this->id_usuario;
			$banco = $_SESSION['con'];
			if (!$banco) {
					die('Não foi possível conectar: ' . mysql_error());
			}

			$sql = 'SELECT c.id as idcatalogacao, r.descricao as descricao_reino, r.*, c.*, u.* from catalogacao c join usuario u on c.id_usuario = u.id join reino r on c.id_reino = r.id where id_usuario = "'.$id_usuario.'"';

			$executa = mysqli_query($banco, $sql);

			return $executa;

	}
}

?>