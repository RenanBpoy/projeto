<?php include('header.php');
include('leftsidebar.php');
include('rightsidebar.php');


$id_user = $_SESSION['usuario']['id'];

if (isset($_GET['id_reino'])){
    $sql = 'SELECT c.id as idcatalogacao, r.descricao as descricao_reino, r.*, c.*, u.* from catalogacao c join usuario u on c.id_usuario = u.id join reino r on c.id_reino = r.id where (id_usuario = "'.$id_user.'") and (id_reino = "'.$_GET["id_reino"].'")';
    
}
else{

$sql = 'SELECT c.id as idcatalogacao, r.descricao as descricao_reino, r.*, c.*, u.* from catalogacao c join usuario u on c.id_usuario = u.id join reino r on c.id_reino = r.id where id_usuario = "'.$id_user.'"';

}


 ?>
        

    <div id="wrapper">


        <section class="section lb">
            <div class="container">

            	<div class="row">
                    <div class="col-md-12">
                        <div class="portfolio row with-desc">

                            <?php 


                            $linhas = mysqli_query($con, $sql);

                            if(mysqli_num_rows($linhas) < 1){?>
                                <h2>Você não possui catalogações desse reino</h2>
                            <?php }
                            while($catalogacao = mysqli_fetch_assoc($linhas)): 
                            $timeStamp = $catalogacao['data_hora_catalogacao'];
                            $timeStamp = date( "d/m/Y", strtotime($timeStamp));

                                ?>

                			<?php include('modelo_catalogacao.php') ?>

                		    <?php endwhile ?>

                        </div>
                    </div>
                </div>
                
            </div><!-- end container -->
        </section><!-- end section -->

        

<?php include('footer.php') ?>