<?php include('header.php');
include('leftsidebar.php');
include('rightsidebar.php');

$usuario = new Usuario();

$novo_nome = md5(time()).'.jpg';
$_SESSION['foto_usuario'] = $novo_nome;


$linhas = mysqli_query($con, 'SELECT * from usuario where id="'.$_SESSION['id_user'].'"');
while($usuario = mysqli_fetch_assoc($linhas)): 



 ?>




    <div id="wrapper">

        <form action="salvar_perfil.php" method="POST">

        <section class="section lb">
            <div class="container">
            	<div class="row">
            		<div class="col-4">

                  <img width="200" height="200" class="circle-image picture"  <?php echo ' src="images/imagem_usuario/'.$usuario['imagem'].'" ' ?>/>

                  
            		</div>
            		<div class="col-4 mt-5">
            			<h1 class="h0"><?php echo $usuario['nome'] ?></h1>
            			<h1>@<?php echo $usuario['username'] ?></h1>
            		</div>
            		<div class="col-4 mt-5">
                <?php 
                if ($_SESSION['id_user'] == $_SESSION['usuario']['id']){ ?>
                <a href="editarperfil.php" class="btn btn-primary button-profile mt-5">Editar perfil</a>
                <?php 
                }
                else{ 
                  $seguir = new Seguir();
                  $seguir->setIdUsuario1($_SESSION['usuario']['id']);
                  $seguir->setIdUsuario2($_SESSION['id_user']);
                  $array = $seguir->consultarSeguir();
                  $amigos = mysqli_fetch_array($array);
                  if($amigos){?>

                    
                    <a <?php echo 'href="deixarseguir.php?id_usuario2='.$_SESSION['id_user'].'"' ?> class="btn btn-danger button-profile mt-5">Deixar de Seguir</a>
                  

                <?php
                  }
                  else{
                ?>
                  <a <?php echo 'href="seguir.php?id_usuario2='.$_SESSION['id_user'].'"' ?> class="btn btn-success button-profile mt-5">Seguir</a>
                  <?php
                  }
                }
                ?>
            			
            		</div>
            	</div>


                <div class="section-title text-center primary-menu">

              <center><nav class="navbar navbar-expand-lg navbar-light">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#conteudoNavbarSuportado" aria-controls="conteudoNavbarSuportado" aria-expanded="false" aria-label="Alterna navegação">
                  <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="conteudoNavbarSuportado">
                  <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                      <a class="nav-link" <?php echo 'href="userprofile.php?id='.$_SESSION['id_user'].'"' ?>>Publicações <span class="sr-only">(página atual)</span></a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" <?php echo 'href="userprofileinformacoes.php?id='.$_SESSION['id_user'].'"' ?>>Perfil <span class="sr-only">(página atual)</span></a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" <?php echo 'href="userprofilecatalogacoes.php?id='.$_SESSION['id_user'].'"' ?>>Catalogações</a>
                    </li>
                    <li class="nav-item active">
                      <a class="nav-link" <?php echo 'href="userprofilepremiacoes.php?id='.$_SESSION['id_user'].'"' ?>>Premiações</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" <?php echo 'href="userprofileseguidores.php?id='.$_SESSION['id_user'].'"' ?>>Seguidores</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" <?php echo 'href="userprofileseguindo.php?id='.$_SESSION['id_user'].'"' ?>>Seguindo</a>
                    </li>
                  </ul>
                </div>
              </nav></center>
                    <hr>
                    <h3>Premiações</h3>
                    <hr>

                <div class="row">
                    <?php

                    $sql = "SELECT * FROM premiacao p JOIN medalha m on m.id = p.id_medalha WHERE p.id_usuario = ".$_SESSION['id_user'];
                    $sql_query = mysqli_query($_SESSION['con'], $sql);
                    if (mysqli_num_rows($sql_query)==0) { ?>
                      <div class="col"><center><h4>Ops... parece que esse usuário não possui premiações</h4></center></div>
                    <?php }
                    else{
                    while($medalhas = mysqli_fetch_assoc($sql_query)):?>
                    <div class="col-3">
                        <img <?php echo 'src="images/imagem_medalha/'.$medalhas['imagem_medalha'].'"' ?> class="img-fluid">
                    </div>
                    <?php endwhile; } ?>

                </div>

                </div><!-- end title -->

<?php
endwhile; ?>




<?php include('footer.php');
?>