<?php include('header.php');
include('leftsidebar.php');
include('rightsidebar.php');

$compartilhamento = new Compartilhamento();

if(isset($_POST['mensagem'])){


$compartilhamento->setId($_SESSION['editar']['id_compartilhamento']);
$compartilhamento->setMensagem($_POST['mensagem']);

$compartilhamento->editarCompartilhamento();
header('Location:/projeto/home.php');
exit();


}

else{
	$_SESSION['editar']['id_compartilhamento'] = $_GET['id'];
}




?>

<div id="wrapper">

	<form method="post">
        <section class="section lb">
            <div class="container">
                <div class="section-title text-center">
                    <h3>Editar compartilhamento</h3>

                </div><!-- end title -->

                <div class="row">
                    <div class="col">

                        <div class="portfolio row with-desc">

                        	<?php 

                            $compartilhamento->setId($_SESSION['editar']['id_compartilhamento']);

                            $sqlquery = $compartilhamento->buscarCompartilhamento();

                        	$catalogacao = mysqli_fetch_assoc($sqlquery);

                    	    $timeStamp = date_default_timezone_get('America/Sao_Paulo');
                            $timeStamp = date( "d/m/Y", strtotime($timeStamp));


                        	?>

                            <div class="post-media pitem item-w1 item-h1 cat1">
                                <div class="item-desc">
                                    <div class="row">
                                        <div class="col-3">
                                            <img <?php echo ' src="images/imagem_usuario/'.$_SESSION['usuario']['imagem'].'" ' ?> class="circle-image img-fluid mt-2">
                                        </div>
                                        <div class="col-7 mt-3 ml-negative">
                                            <h4><?php echo $_SESSION['usuario']['nome'] ?></h4>
                                        </div>
                                        <div class="col-1 mt-3">
                                            <a <?php echo 'href="deletarcompartilhamento.php?id='.$_SESSION['editar']['id_compartilhamento'].'"' ?> class="btn-danger golinkdanger"><i class="fa fa-trash"></i></a>
                                        </div>
                                        
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-12 maxrow">
                                              <div class="form-group">
											    <textarea name="mensagem" class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Escreva sua mensagem"><?php echo $catalogacao['mensagem'] ?></textarea>
											  </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="entry">

                                    <a href="#"><img <?php echo ' src="images/imagem_catalogacao/'.$catalogacao['imagem_catalogacao'].'" ' ?> alt="" class="img-responsive img-fluid">

                                        
                                    </a>
                                </div><!-- end entry -->
                                <div class="item-desc">

                                    <div class="row mt-2">
                                        <div class="col-12">
                                            <h3 class="mt-4"><center><?php echo $catalogacao['nome_popular'] ?></center> </h3>
                                            <div class="col-3">
                                                
                                            </div>
                                        </div>
                                    </div>  
                                    <div class="row mt">
                                        <div class="col-12">
                                            <center><h5><i><?php echo $catalogacao['especie'] ?></i></h5></center>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-12">
                                            <center><button type="submit" class="btn btn-primary">Salvar</button></center>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- end post-media -->


                        </div><!-- end row -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end section -->
    </form>

        



<?php include('footer.php') ?>