-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 26-Nov-2019 às 19:26
-- Versão do servidor: 5.7.10-log
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `biocense`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `catalogacao`
--

CREATE TABLE IF NOT EXISTS `catalogacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_popular` varchar(50) NOT NULL,
  `filo` varchar(50) NOT NULL,
  `classe` varchar(50) NOT NULL,
  `ordem` varchar(50) NOT NULL,
  `familia` varchar(50) NOT NULL,
  `genero` varchar(50) NOT NULL,
  `especie` varchar(50) NOT NULL,
  `observacao` varchar(2000) NOT NULL,
  `imagem_catalogacao` varchar(2000) NOT NULL,
  `descricao` varchar(2000) NOT NULL,
  `data_hora_catalogacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_usuario` int(11) NOT NULL,
  `id_reino` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_usuario` (`id_usuario`),
  KEY `id_reino` (`id_reino`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=80 ;

--
-- Extraindo dados da tabela `catalogacao`
--

INSERT INTO `catalogacao` (`id`, `nome_popular`, `filo`, `classe`, `ordem`, `familia`, `genero`, `especie`, `observacao`, `imagem_catalogacao`, `descricao`, `data_hora_catalogacao`, `id_usuario`, `id_reino`) VALUES
(13, 'Elefante-asiático', 'Chordata', 'Mammalia', 'Proboscidea', 'Elephantidae', 'Elephas', 'Elephas maximus', '', '48cba96db82d52c7d8595b941278eb54.jpg', 'O elefante-asiático (Elephas maximus) é a única espécie viva do gênero Elephas. Ocorre no sudeste asiático da Índia e Nepal, até o oeste e leste de Bornéu. Três subespécies são reconhecidas atualmente. Os elefantes asiáticos são os maiores animais terrestres vivos da Ásia.', '2019-11-18 11:02:01', 1, 5),
(14, 'Tartaruga-comum', 'Chordata', 'Reptilia', 'Testudinata', 'Cheloniidae', 'Caretta', 'Caretta caretta', '', 'e0acc1d399b6d779b6d9a60c9e9493e3.jpg', '', '2019-10-22 18:59:40', 1, 5),
(15, 'Amanita muscaria', 'Basidiomycota', 'Homobasidiomycetes', 'Agaricales', 'Amanitaceae', 'Amanita', 'Amanita muscaria', '', 'bf6556033e8dd2c1f7f09c75c124eb69.jpg', 'Fungi', '2019-11-18 11:26:25', 8, 3),
(17, 'Nepente-Rajá de Bornéu', 'Magnoliophyta', 'Magnoliopsida', 'Caryophyllales', 'Nepenthaceae', 'Nepenthes', 'Nepenthes rajah', '', '8a19f2e9ba2bf6faef61194ebb34711b.jpg', 'Nepenthes rajah é uma planta carnívora da família das nepentáceas. É endêmica do Monte Kinabalu e também do Monte Tambuyukon em Sabah, Bornéu. N. rajah cresce exclusivamente sobre substratos de serpentina, particularmente em áreas de infiltração de água do solo onde o solo é solto e permanentemente úmido. A espécie vive numa faixa de 1.500 a 2.650 m de altitude e é considerada como uma planta altiplana ou sub-alpina. Devido à sua distribuição localizada, N. rajah é classificada como uma espécie em perigo pela IUCN e listado no Anexo I do CITES.', '2019-10-22 19:00:24', 2, 4),
(18, 'Flamingo', 'Chordata', 'Aves', 'Phoenicopteriformes', 'Phoenicopteridae', 'Phoenicopterus', 'Phoenicopterus roseus', '', '02d2b177d57658e87644738c829941a2.jpg', '', '2019-10-24 12:56:12', 7, 5),
(19, 'Ornitorrinco', 'Chordata', 'Mammalia', 'Monotremata', 'Ornithorhynchidae', 'Ornithorhynchus', 'Ornithorhynchus anatinus', '', '5a1d2c73626c422c2c341e59608659f1.jpg', '', '2019-10-24 14:56:31', 8, 5),
(20, 'Axolotle', 'Chordata', 'Amphibia', 'Caudata', 'Ambystomatidae', 'Ambystoma', 'Ambystoma mexicanum', '', '793951f158274fe8b5a1f004da7e27f3.jpg', '', '2019-10-29 18:47:58', 8, 5),
(21, 'Lobo-do-ártico', 'Chordata', 'Mammalia', 'Carnivora', 'Canidae', 'Canis', 'Canis lupus', '', 'ebdac522a7134e5fe022eb49c4372bdd.jpg', '', '2019-10-29 18:52:40', 7, 5),
(22, 'Gato siamês', 'Chordata', 'Mammalia', 'Carnivora', 'Felidae', 'Felis', 'Felis catus', '', 'c4c5683eeaa5856e8c5fe196a1593b2e.jpg', '', '2019-10-29 19:13:41', 7, 5),
(23, 'Bagre-africano', 'Chordata', 'Actinopterygii', 'Siluriformes', 'Clariidae', 'Clarias', 'Clarias gariepinus', '', '20465e8e953740cfa3c3eefd71cb7c03.jpg', '', '2019-11-05 01:03:30', 1, 5),
(25, 'Cirurgião-patela', 'Chordata', 'Actinopterygii', 'Perciformes', 'Acanthuridae', 'Paracanthurus', 'Paracanthurus hepatus', '', 'e5c66a5b857f823b54a9fb9a0b7ff8c9.jpg', '', '2019-10-29 19:28:27', 7, 5),
(26, 'Peixe-palhaço', 'Chordata', 'Actinopterygii', 'Perciformes', 'Pomacentridae', 'Amphiprion', 'Amphiprion ocellaris.', '', 'de1cfa39dd78471163a8dfd5cbb5092f.jpg', '', '2019-11-05 19:11:22', 7, 5),
(29, 'Tigre-siberiano', 'Chordata', 'Mammalia', 'Carnivora', 'Felidae', 'Panthera', 'Panthera tigris', '', '8247dc1d11c4e1954d68ff886d68677f.jpg', 'O tigre-siberiano, também conhecido como tigre-de-amur, é uma das 6 populações de tigre ainda existentes. É a população de tigre mais setentrional e a maior de todas. Habita áreas próximas ao Rio Amur na Sibéria no extremo leste da Rússia, e uma pequena área no nordeste da China.', '2019-11-05 00:47:17', 1, 5),
(30, 'Orca', 'Chordata', 'Mammalia', 'Cetacea', 'Delphinidae', 'Orcinus', 'Orcinus orca', '', '89e18c82e9c67ba10f4e27561363b3fd.jpg', 'A Orca (Orcinus orca) é o membro da família dos golfinhos de maior porte e é um superpredador versátil, que inclui na sua dieta presas como peixes, moluscos, aves, tartarugas, focas, tubarões e animais de tamanho maior quando caçam em grupo, como por exemplo baleias. Apesar de “baleia-assassina” ser uma designação incorreta, por ser uma tradução direta do inglês “killer whale”, e pelo facto de o animal não ser uma baleia, ela é comumente usada.', '2019-10-14 03:00:00', 1, 5),
(37, 'Mamba-negra', 'Chordata', 'Reptilia', 'Serpentes', 'Elapidae', 'Dendroaspis', 'Dendroaspis polylepis', '', '3e56f162c8d20b5b3b07306be874de53.jpg', 'A Mamba-negra (Dendroaspis polylepis) é uma das cobras mais venenosas do continente africano. É a cobra mais rápida do mundo, capaz de se deslocar a 20 km/h. É a cobra mais rápida do mundo, capaz de se deslocar a 20 km/h. Porém, usa essa velocidade para escapar do perigo e para atacar as suas presas.', '2019-11-05 18:35:36', 1, 5),
(39, 'Pavão-Verde', 'Chordata', 'Aves', 'Galliformes', 'Phasianidae', 'Pavo', 'Pavo muticus', 'Linda e majestosa ave!!!', '2d1d3c973db3c39a65b6aa90de79d55f.jpg', 'O pavão-verde (Pavo muticus) é uma espécie pertencente ao gênero Pavo da família Phasianidae. Trata-se de uma ave nativa do Sudeste Asiático.', '2019-11-05 18:48:17', 2, 5),
(40, 'Bactéria do Tétano', 'Firmicutes', 'Clostridia', 'Clostridiales', 'Clostridiaceae', 'Clostridium', 'Clostridium tetani', 'Já contrai ela... :(', 'fe8d2f610c1455b63fa6c90c5788e2a3.jpg', 'Clostridium tetani é uma bactéria Gram-positiva e anaeróbica do gênero Clostridium. Durante seu crescimento, os bacilos possuem flagelos abundantes com movimentos lentos. Duas principais toxinas, a Tetanospasmina (vulgarmente chamada toxina do tétano) e tetanolisina são produzidas durante esta fase. Tetanospasmina é codificada em um plasmídeo, que está presente em todas as cepas toxigênicas. Já a tetanolisina tem a importância incerta na patogênese do tétano. Quando já maduros, o C. tetani perde seus flagelos, desenvolve um esporo terminal e se assemelha a uma “raquete de squash” (como visto na figura). Os esporos são extremamente estáveis no ambiente, mantendo a capacidade para germinar e causar doença indefinidamente. Eles suportam a exposição ao etanol, ao fenol ou à formalina, entretanto, ao ser exposto ao iodo, ao glutaraldeído, ao peróxido de hidrogênio ou à autoclavagem a 121 ºC e 103 kPa (15 psi) durante 15 minutos o esporo perde sua capacidade infecciosa . Crescimento adequado em cultura se dá a 37 ° C e sob condições estritamente anaeróbicas. A sensibilidade in vitro do C. tetani inclui metronidazol, penicilinas, cefalosporinas, imipenem, macrolídeos e tetraciclina.', '2019-11-05 18:53:04', 11, 1),
(41, 'Tatuzinho de jardim', 'Chordata', 'Malacostraca', 'Isopoda', 'Philosciidae', 'Atlantoscia', 'Atlantoscia floridana', '', 'f4c7f25b5ee3038163a8b04a9d1a515c.jpg', '', '2019-11-05 19:06:07', 12, 5),
(46, 'Águia-Careca', 'Chordata', 'Aves', 'Accipitriformes', 'Accipitridae', 'Haliaeetus', 'Haliaeetus leucocephalus', '', 'ef1d04e18e3435850eab2407cbd095f6.jpg', 'A águia-de-cabeça-branca, águia-careca, águia-americana ou pigargo-americano (Haliaeetus leucocephalus) é uma águia nativa da América do Norte. Sua distribuição geográfica inclui a maioria do Canadá e Alasca, todos os Estados Unidos contíguos, e norte do México. Encontra-se perto de grandes corpos de águas abertas com abundância de alimento e árvores antigas para o nidificação.', '2019-11-06 17:24:12', 2, 5),
(51, 'Tiranossauro Rex', 'Chordata', 'Reptilia', 'Saurischia', 'Tyrannossauridae', 'Tyrannossaurus', 'Tyrannosaurus imperiosus', '', '86fb13e76fe27af6017519e74c5a769b.jpg', '', '2019-10-15 21:09:03', 1, 5),
(52, 'Golfinho', 'Chordata', 'Mammalia', 'Cetacea', 'Delphinidae', 'Delphinus', 'Delphinus delphis', '', '6c76e49abe76143aa9600507feab8c90.jpg', '', '2019-11-07 21:13:32', 1, 5),
(53, 'Crocodilo-americano', 'Chordata', 'Reptilia', 'Crocodylia', 'Crocodylidae', 'Crocodylus', 'Crocodylus Acutus', '', '598da9cc6113fe8b60398e3a655cb86e.jpg', '', '2019-11-07 21:17:30', 1, 5),
(55, 'Papagaio-verdadeiro', 'Chordata', 'Aves', 'Psitaciformes', 'Psittacidae', 'Amazona', 'Amazona aestiva', '', '1d64582508b5bbbbc971fd5b1817c6db.jpg', '', '2019-11-07 22:32:18', 1, 5),
(56, 'Cavalo', 'Chordata', 'Mammalia', 'Perissodactyla', 'Equidae', 'Equus', 'Equus ferus', '', '96978cad8a54a7e8bc7c2cd7b6a8977c.jpg', '', '2019-11-11 11:14:39', 7, 5),
(57, 'Peixe Mandarim', 'Chordata', 'Actinopterygii', 'Perciformes', 'Callionymidae', 'Synchiropus', 'S. splendidus', '', '3d736683f737638aefb9bcc157ff8255.jpg', '', '2019-11-11 11:17:40', 7, 5),
(58, 'Leão', 'Chordata', 'Mammalia', 'Carnivora', 'Felidae', 'Panthera', 'Panthera leo', '', 'fda98eb01d52226c5e4b54acb6bb57d1.jpg', '', '2019-11-11 11:21:20', 7, 5),
(59, 'Panda-gigante', 'Chordata', 'Mammalia', 'Carnivora', 'Ursidae', 'Ailuropoda', 'Ailuropoda melanoleuca', '', '935ac9be4bfe41f0108c8b54527133a7.jpg', '', '2019-11-11 11:25:27', 7, 5),
(60, 'Arara-azul-grande', 'Chordata', 'Aves', 'Psittaciformes', 'Psittacidae', 'Anodorhynchus', 'Anodorhynchus hyacinthinus', '', 'd7a582e78b36fc6ca9fd2ec9c7dabf92.jpg', '', '2019-11-11 11:29:37', 7, 5),
(61, 'Tigre-Dente-de-Sabre', 'Chordata', 'Mammalia', 'Carnivora', 'Felidae', 'Smilodon', 'Smilodon fatalis', '', '1673508923cf3660e30701bc2ea1f671.jpg', '', '2019-11-11 12:38:32', 7, 5),
(62, 'Rainha-de-Madagáscar', 'Arthropoda', 'Insecta', 'Lepidoptera', 'Uraniidae', 'Chrysiridia', 'Chrysiridia rhipheus', '', '9a5a6b0bec820645d00728104d50f19a.jpg', '', '2019-11-11 11:39:49', 7, 5),
(63, 'Onça-preta', 'Chordata', 'Mammalia', 'Carnivora', 'Felidae', 'Panthera', 'Panthera onca', '', '0d0ce894dea4d335c1aa6101ca5ed1c1.jpg', '', '2019-11-11 12:15:23', 7, 5),
(64, 'Guepardo', 'Chordata', 'Mammalia', 'Carnivora', 'Felidae', 'Acinonyx', 'Acinonyx jubatus', '', '11983929446b1471c6b34dbd8604bd45.jpg', '', '2019-11-11 12:19:26', 7, 5),
(65, 'Leopardo', 'Chordata', 'Mammalia', 'Carnivora', 'Felidae', 'Panthera', 'Panthera pardus', '', 'a793e2f6f75f7a67dec07631932dcc80.jpg', '', '2019-11-11 12:27:45', 7, 5),
(66, 'Leopardo-das-neves', 'Chordata', 'Mammalia', 'Carnivora', 'Felidae', 'Panthera', 'Panthera uncia', '', 'ed5a98f746cc75bef7c693ed4c802d20.jpg', '', '2019-11-11 12:32:34', 7, 5),
(67, 'Lince', 'Chordata', 'Mammalia', 'Carnivora', 'Felidae', 'Lynx', 'Lynx canadensis', '', '773f5f09f3aaa8796b04dfa8d114fc9f.jpg', '', '2019-11-11 12:36:26', 7, 5),
(68, 'Leão-americano', 'Chordata', 'Mammalia', 'Carnivora', 'Felidae', 'Panthera', 'Panthera leo atrox', '', '6a9bb275ce483c656c4512f52b5d0323.jpg', '', '2019-11-18 12:00:45', 7, 5),
(69, 'Peixe-boi', 'Chordata', 'Mammalia', 'Sirenia', 'Trichechidae', 'Trichechus', 'Trichechus inunguis', '', '44f754f4599feaff8aa7623ce62035ca.jpg', '', '2019-11-11 12:51:22', 7, 5),
(70, 'Mico-leão dourado', 'Chordata', 'Mammalia', 'Primates', 'Callitrichidae', 'Leontopithecus', 'Leontopithecus rosalia', '', '6f09d702188cfa9d490722fe7f710d12.jpg', '', '2019-11-11 12:54:10', 7, 5),
(71, 'Tamanduá-bandeira', 'Chordata', 'Mammalia', 'Pilosa', 'Myrmecophagidae', 'Myrmecophaga', 'Myrmecophaga tridactyla', '', '1edb868b7e7ef2498e11cab02f143591.jpg', '', '2019-11-11 12:56:28', 7, 5),
(72, 'Tatu-canastra', 'Chordata', 'Mammalia', 'Cingulata', 'Chlamyphoridae', 'Priodontes', 'Priodontes maximus', '', '5e6d904b4b76c756e68e52eeac6d59c0.jpg', '', '2019-11-11 12:58:14', 7, 5),
(73, 'Beija-flor', 'Chordata', 'Aves', 'Apodiformes', 'Trochilidae', 'Phaethornis', 'Phaethornis koepckeae', '', 'b6e2b6675df24c88a4f36854619e1818.jpg', '', '2019-11-11 13:02:14', 7, 5),
(74, 'Boto-cor-de-rosa', 'Chordata', 'Mammalia', 'Cetacea', 'Iniidae', 'Inia', 'Inia geoffrensis', '', 'b0c7ea214b1d79ed9c9397c0e0a188fc.jpg', '', '2019-11-11 13:07:50', 7, 5),
(75, 'Tigre-de-Bengala', 'Chordata', 'Mammalia', 'Carnivora', 'Felidae', 'Panthera', 'Panthera tigris', '', 'c49363566570e3956404e096eec691ac.jpg', '', '2019-11-11 13:09:41', 7, 5),
(76, 'Tulipa', 'Magnoliophyta', 'Liliopsida', 'Liliales', 'Liliaceae', 'Tulipa', 'Tulipa praestans', '', 'd9923d4b5fb21fea1b4b37661def139f.jpg', 'Com cerca de cem da folhagem surge uma haste ereta, com flor solitária formada por seis pétalas. Cores e formas são bem variadas. Existem muitas variedades cultivadas e milhares de híbridos em diversas cores, tons matizados, pontas picotadas, etc. O bulbo contém alcaloides termoestáveis e cristais de oxalado de cálcio. Manipulados libertam um pó que pode provocar conjuntivites, rinites e até crises de asma.', '2019-11-11 14:57:33', 21, 4),
(77, 'Pug', 'pug', 'pug', 'pug', 'pug', 'pug', 'pug', '', '03691ad45167a880b0107e3c476bafd5.jpg', '', '2019-11-18 11:33:09', 8, 5),
(78, 'Faisão Dourado', 'Chordata', 'Aves', 'Galliformes', 'Phasianidae', 'Chrysolophus', 'Chrysolophus pictus', '', '28aaa59f4ccfe2d848b498297e96c5fc.jpg', '', '2019-11-18 11:44:02', 7, 5),
(79, 'Esquilo', 'Chordata', 'Mammalia', 'Rodentia', 'Sciuridae', 'Rheithrosciurus', 'Rheithrosciurus Gray', 'Bonitinho', '416e83ad14aebf2da358a7a285e6d5c4.jpg', 'As sementes são as principais fontes de alimentação, mas também consomem insetos e frutas. Quando coletam alimento, enterram algumas sementes que encontram, sendo que algumas chegam a germinar, como pinhões e coquinhos, acabando por plantar árvores como araucária e jerivá.', '2019-11-26 14:30:58', 22, 5);

-- --------------------------------------------------------

--
-- Estrutura da tabela `compartilhamento`
--

CREATE TABLE IF NOT EXISTS `compartilhamento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_hora_compartilhamento` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `mensagem` varchar(1000) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_catalogacao` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_usuario` (`id_usuario`),
  KEY `id_catalogacao` (`id_catalogacao`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=88 ;

--
-- Extraindo dados da tabela `compartilhamento`
--

INSERT INTO `compartilhamento` (`id`, `data_hora_compartilhamento`, `mensagem`, `id_usuario`, `id_catalogacao`) VALUES
(57, '2019-11-26 13:46:54', 'Incrível espécie', 1, 13),
(60, '2019-11-18 11:24:40', 'Quase mais lindo que o Renan', 7, 18),
(61, '2019-11-18 11:44:52', 'AAAAUUUUUUUUUU', 7, 21),
(62, '2019-11-18 11:45:05', 'MIauuu', 7, 22),
(63, '2019-11-18 11:45:26', 'Neminho', 7, 26),
(64, '2019-11-18 11:45:43', 'Amigo do neminho', 7, 25),
(65, '2019-11-18 11:48:37', 'Pocoto pocoto pocoto...', 7, 56),
(66, '2019-11-18 11:47:07', 'Raaaauuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu', 7, 58),
(67, '2019-11-18 11:49:05', 'Me da um ABRAÇO de Urso!', 7, 59),
(68, '2019-11-18 11:49:40', 'SOU Bonito e nada mais importa!', 7, 57),
(69, '2019-11-18 11:54:13', 'AAAAAAAAAAHAAAAAAAAAAAAA estilo Hélder', 7, 60),
(70, '2019-11-18 11:50:40', 'Veneno', 7, 62),
(71, '2019-11-18 11:51:54', 'O cara mais nebuloso do mundo que não existe mais', 7, 61),
(72, '2019-11-18 11:52:19', 'Meus olhos são que nem o da Valentina', 7, 63),
(73, '2019-11-18 11:52:59', 'Quem é Usain Bolt?', 7, 64),
(74, '2019-11-18 11:53:28', 'Meu nome é Léo', 7, 65),
(75, '2019-11-18 11:55:50', 'Meu nome é Léo das Neves', 7, 66),
(76, '2019-11-18 11:57:15', 'Alince Alince menina dos olhos teus! \r\n', 7, 67),
(77, '2019-11-18 11:57:40', 'Rauuuuuuuuuuuuuuuuuuuuuuuuu dos USA', 7, 68),
(78, '2019-11-18 11:58:07', 'Isso é Amor', 7, 75),
(79, '2019-11-18 12:03:26', 'Ta rolando amor, encontro de metades da Rosa e do...', 7, 73),
(80, '2019-11-18 12:01:49', 'Mas nem vou falar quem é...kkkk', 7, 69),
(81, '2019-11-18 12:02:33', 'Muito Bot', 7, 74),
(82, '2019-11-18 12:04:08', 'Não é Truco gurizada', 7, 72),
(83, '2019-11-24 16:28:51', 'Serpente majestosa!', 1, 37),
(84, '2019-11-24 16:30:52', 'Lindo!!!', 8, 19),
(85, '2019-11-26 13:44:42', 'Exótico!', 8, 20),
(86, '2019-11-24 18:07:45', 'Ave maravilhosa!', 2, 46),
(87, '2019-11-26 14:32:05', 'Lindo Esquilooo!!', 22, 79);

-- --------------------------------------------------------

--
-- Estrutura da tabela `medalha`
--

CREATE TABLE IF NOT EXISTS `medalha` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(20) NOT NULL,
  `imagem_medalha` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `medalha`
--

INSERT INTO `medalha` (`id`, `descricao`, `imagem_medalha`) VALUES
(1, 'Bronze', 'bronze.png'),
(2, 'Prata', 'prata.png'),
(3, 'Ouro', 'ouro.png'),
(4, 'Diamante', 'diamante.png');

-- --------------------------------------------------------

--
-- Estrutura da tabela `premiacao`
--

CREATE TABLE IF NOT EXISTS `premiacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_hora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_usuario` int(11) NOT NULL,
  `id_medalha` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Extraindo dados da tabela `premiacao`
--

INSERT INTO `premiacao` (`id`, `data_hora`, `id_usuario`, `id_medalha`) VALUES
(4, '2019-11-07 21:19:24', 1, 1),
(5, '2019-11-11 11:29:38', 7, 1),
(6, '2019-11-11 14:06:28', 7, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `reino`
--

CREATE TABLE IF NOT EXISTS `reino` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `reino`
--

INSERT INTO `reino` (`id`, `descricao`) VALUES
(1, 'Monera'),
(2, 'Protoctista'),
(3, 'Fungi'),
(4, 'Plantae'),
(5, 'Animalia');

-- --------------------------------------------------------

--
-- Estrutura da tabela `seguir`
--

CREATE TABLE IF NOT EXISTS `seguir` (
  `id_usuario1` int(11) NOT NULL,
  `id_usuario2` int(11) NOT NULL,
  `data_hora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `seguir`
--

INSERT INTO `seguir` (`id_usuario1`, `id_usuario2`, `data_hora`) VALUES
(8, 7, '2019-11-07 23:09:09'),
(8, 1, '2019-11-17 00:12:19'),
(18, 1, '2019-11-09 16:06:44'),
(2, 1, '2019-11-06 17:12:37'),
(2, 12, '2019-11-06 17:12:46'),
(2, 11, '2019-11-06 17:13:03'),
(2, 7, '2019-11-06 17:13:05'),
(2, 8, '2019-11-06 17:13:07'),
(1, 11, '2019-11-26 14:07:12'),
(22, 1, '2019-11-26 14:31:33'),
(1, 12, '2019-11-08 02:11:02'),
(18, 7, '2019-11-09 16:07:14'),
(1, 2, '2019-11-11 11:05:26'),
(7, 2, '2019-11-11 11:08:21'),
(7, 1, '2019-11-11 11:08:24'),
(7, 8, '2019-11-11 11:08:26'),
(7, 12, '2019-11-11 11:08:28'),
(21, 8, '2019-11-11 14:53:08'),
(8, 2, '2019-11-17 00:09:57'),
(1, 8, '2019-11-18 11:15:00'),
(1, 7, '2019-11-26 14:35:28'),
(22, 12, '2019-11-26 14:42:16'),
(1, 22, '2019-11-26 14:43:27');

-- --------------------------------------------------------

--
-- Estrutura da tabela `sexo`
--

CREATE TABLE IF NOT EXISTS `sexo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `sexo`
--

INSERT INTO `sexo` (`id`, `descricao`) VALUES
(1, 'Masculino'),
(2, 'Feminino');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `data_nascimento` date NOT NULL,
  `email` varchar(200) NOT NULL,
  `senha` varchar(2000) NOT NULL,
  `username` varchar(50) NOT NULL,
  `imagem` varchar(2000) DEFAULT 'avatar.png',
  `sobrenome` varchar(40) NOT NULL,
  `crbio` varchar(20) NOT NULL,
  `data_hora_cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_sexo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sexo` (`id_sexo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id`, `nome`, `data_nascimento`, `email`, `senha`, `username`, `imagem`, `sobrenome`, `crbio`, `data_hora_cadastro`, `id_sexo`) VALUES
(1, 'Renan', '2002-03-11', 'renanbp2@gmail.com', '784ca0c5cb64afe59ff1c875ac71fc10', 'RenanBpoy', 'd44e7365202c90ab312318eb571a89bf.jpg', 'Bordignon Poy', '', '2019-11-24 18:06:32', 1),
(2, 'Lucas', '2002-02-03', 'lucasalenhardt@gmail.com', '25f9e794323b453885f5181f1b624d0b', 'lucasalenhardt', '59dc27ff48bdf2613f5980443fc7099f.jpg', 'Adamy Lenhardt', '', '2019-10-21 14:15:07', 1),
(7, 'Almyr Heitor', '2002-08-28', 'almyrheitorpg@gmail.com', '316d931e603875eb1d03a9ebc4415a2f', 'almyrgodoy', 'd1d9be2e3babd0121d9902a46cdcdcee.jpg', 'Prediger Godoy', '', '2019-11-18 11:24:01', 1),
(8, 'Valentina', '2003-01-14', 'valentinabeituni@hotmail.com', '629d2d1f161407aa53e6b57e9b47c02c', 'valentinabeituni', '6b7a82f249ba12ba6577a144e2acad22.jpg', 'Beituni', '', '2019-10-31 17:15:23', 2),
(11, 'Stefany ', '2002-08-29', 'guria797@gmail.com', '202cb962ac59075b964b07152d234b70', 'stefh_m', 'b11a0004350d98f4ee8dbc120343325a.jpg', 'Pranke', '', '2019-10-22 19:31:18', 2),
(12, 'Bianca', '1987-12-29', 'bia.lais@gmail.com', '3e2459915a8623f5eba4faeb2b8bbb6a', 'biancalz', 'fd50e9f63c4016a9e0348a67664ef98b.jpg', 'Zimmermann', '', '2019-10-22 19:38:08', 2),
(21, 'Marina', '1992-10-01', 'marina.girolimetto@ibiruba.ifrs.edu.br', '698dc19d489c4e4db73e28a713eab07b', 'marinagirolimetto', 'avatar.png', 'Girolimetto', '', '2019-11-11 14:50:12', 2),
(22, 'David Henrique ', '2003-05-11', 'davidhrifrs@gmail.com', '202cb962ac59075b964b07152d234b70', 'DavidHR', '64bdccff43ec5b12d22584590242c151.jpg', 'Rohr', '', '2019-11-26 14:40:08', 1);

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `catalogacao`
--
ALTER TABLE `catalogacao`
  ADD CONSTRAINT `catalogacao_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `catalogacao_ibfk_2` FOREIGN KEY (`id_reino`) REFERENCES `reino` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `compartilhamento`
--
ALTER TABLE `compartilhamento`
  ADD CONSTRAINT `compartilhamento_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `compartilhamento_ibfk_2` FOREIGN KEY (`id_catalogacao`) REFERENCES `catalogacao` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`id_sexo`) REFERENCES `sexo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
