<?php
include('header.php');
include('leftsidebar.php');
include('rightsidebar.php');



$sqltw = mysqli_query($_SESSION['con'], 'SELECT COUNT(*) as numero FROM catalogacao c JOIN usuario u on c.id_usuario = u.id WHERE data_hora_catalogacao >= ADDDATE(NOW(), -7) AND data_hora_catalogacao < NOW() AND u.id = "'.$_SESSION["usuario"]["id"].'"');
$poststw = mysqli_fetch_assoc($sqltw);

$sql1w = mysqli_query($_SESSION['con'], 'SELECT COUNT(*) as numero FROM catalogacao c JOIN usuario u on c.id_usuario = u.id WHERE data_hora_catalogacao >= ADDDATE(NOW(), -14) AND data_hora_catalogacao < ADDDATE(NOW(), -7) AND u.id = "'.$_SESSION["usuario"]["id"].'"');
$posts1w = mysqli_fetch_assoc($sql1w);

$sql2w = mysqli_query($_SESSION['con'], 'SELECT COUNT(*) as numero FROM catalogacao c JOIN usuario u on c.id_usuario = u.id WHERE data_hora_catalogacao >= ADDDATE(NOW(), -21) AND data_hora_catalogacao < ADDDATE(NOW(), -14) AND u.id = "'.$_SESSION["usuario"]["id"].'"');
$posts2w = mysqli_fetch_assoc($sql2w);

$sql3w = mysqli_query($_SESSION['con'], 'SELECT COUNT(*) as numero FROM catalogacao c JOIN usuario u on c.id_usuario = u.id WHERE data_hora_catalogacao >= ADDDATE(NOW(), -28) AND data_hora_catalogacao < ADDDATE(NOW(), -21) AND u.id = "'.$_SESSION["usuario"]["id"].'"');
$posts3w = mysqli_fetch_assoc($sql3w);

$sqlm = mysqli_query($_SESSION['con'], 'SELECT COUNT(*) as numero FROM catalogacao c JOIN usuario u on c.id_usuario = u.id WHERE data_hora_catalogacao >= ADDDATE(NOW(), -35) AND data_hora_catalogacao < ADDDATE(NOW(), -28) AND u.id = "'.$_SESSION["usuario"]["id"].'"');
$postsm = mysqli_fetch_assoc($sqlm);

$dataGeral = array(
    array("label"=> "4 semanas atrás", "y"=> $postsm['numero']),
    array("label"=> "3 semanas atrás", "y"=> $posts3w['numero']),
    array("label"=> "2 semanas atrás", "y"=> $posts2w['numero']),
    array("label"=> "1 semana atrás", "y"=> $posts1w['numero']),
    array("label"=> "Essa semana", "y"=> $poststw['numero'])
);

    
$dataMonera = array(
    array("label"=> "1 mês atrás", "y"=> 8),
    array("label"=> "3 semanas atrás", "y"=> 3),
    array("label"=> "2 semanas atrás", "y"=> 5),
    array("label"=> "1 semana atrás", "y"=> 9),
    array("label"=> "Essa semana", "y"=> 1)
);

$dataProtoctista = array(
    array("label"=> "1 mês atrás", "y"=> 5),
    array("label"=> "3 semanas atrás", "y"=> 3),
    array("label"=> "2 semanas atrás", "y"=> 0),
    array("label"=> "1 semana atrás", "y"=> 2),
    array("label"=> "Essa semana", "y"=> 1)
);

$dataFungi = array(
    array("label"=> "1 mês atrás", "y"=> 5),
    array("label"=> "3 semanas atrás", "y"=> 3),
    array("label"=> "2 semanas atrás", "y"=> 0),
    array("label"=> "1 semana atrás", "y"=> 2),
    array("label"=> "Essa semana", "y"=> 1)
);

$dataPlantae = array(
    array("label"=> "1 mês atrás", "y"=> 5),
    array("label"=> "3 semanas atrás", "y"=> 3),
    array("label"=> "2 semanas atrás", "y"=> 0),
    array("label"=> "1 semana atrás", "y"=> 2),
    array("label"=> "Essa semana", "y"=> 1)
);

$dataAnimalia = array(
    array("label"=> "1 mês atrás", "y"=> 5),
    array("label"=> "3 semanas atrás", "y"=> 3),
    array("label"=> "2 semanas atrás", "y"=> 0),
    array("label"=> "1 semana atrás", "y"=> 2),
    array("label"=> "Essa semana", "y"=> 1)
);



?>

    <script>
        window.onload = function () {
         
        var chart = new CanvasJS.Chart("chartGeral", {
            animationEnabled: true,
            theme: "light2",
            title:{
                text: ""
            },

            data: [{
                type: "column",

                dataPoints: <?php echo json_encode($dataGeral, JSON_NUMERIC_CHECK); ?>
            }]
        });
        chart.render();

        var chart = new CanvasJS.Chart("chartMonera", {
            animationEnabled: true,
            theme: "light2",
            title:{
                text: "Monera"
            },

            data: [{
                type: "column",

                dataPoints: <?php echo json_encode($dataMonera, JSON_NUMERIC_CHECK); ?>
            }]
        });
        chart.render();

        var chart = new CanvasJS.Chart("chartProtoctista", {
            animationEnabled: true,
            theme: "light2",
            title:{
                text: "Protoctista"
            },

            data: [{
                type: "column",

                dataPoints: <?php echo json_encode($dataProtoctista, JSON_NUMERIC_CHECK); ?>
            }]
        });
        chart.render();

        var chart = new CanvasJS.Chart("chartFungi", {
            animationEnabled: true,
            theme: "light2",
            title:{
                text: "Fungi"
            },

            data: [{
                type: "column",

                dataPoints: <?php echo json_encode($dataFungi, JSON_NUMERIC_CHECK); ?>
            }]
        });
        chart.render();

        var chart = new CanvasJS.Chart("chartPlantae", {
            animationEnabled: true,
            theme: "light2",
            title:{
                text: "Plantae"
            },

            data: [{
                type: "column",

                dataPoints: <?php echo json_encode($dataPlantae, JSON_NUMERIC_CHECK); ?>
            }]
        });
        chart.render();

        var chart = new CanvasJS.Chart("chartAnimalia", {
            animationEnabled: true,
            theme: "light2",
            title:{
                text: "Animalia"
            },

            data: [{
                type: "column",

                dataPoints: <?php echo json_encode($dataAnimalia, JSON_NUMERIC_CHECK); ?>
            }]
        });
        chart.render();
         

         
        }
    </script>
        


    <div id="wrapper">

    <?php 
    // date_default_timezone_set('America/Sao_Paulo');
    // $date = date('m/d/Y', time());
    // echo $date;

    ?>

        <section class="section lb">
            <div class="container">
                <div class="section-title text-center">
                    <h3>Catalogações mensais</h3>
                    <hr>
<!--                     <p>Maecenas sit amet tristique turpis. Quisque porttitor eros quis leo pulvinar, at hendrerit sapien iaculis. Donec consectetur accumsan arcu, sit amet fringilla ex ultricies.</p> -->
                </div><!-- end title -->

                <div class="row">
                    <div class="col-md-12">
                        <div  id="chartGeral" style="height: 370px; width: 100%;"></div>
                        <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
                    </div><!-- end col -->
<!--                     <div class="col-md-12">
                        <div id="chartMonera" style="height: 370px; width: 100%;"></div>
                        <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
                    </div>
                    <div class="col-md-12">
                        <div id="chartProtoctista" style="height: 370px; width: 100%;"></div>
                        <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
                    </div>
                    <div class="col-md-12">
                        <div id="chartFungi" style="height: 370px; width: 100%;"></div>
                        <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
                    </div>
                    <div class="col-md-12">
                        <div id="chartPlantae" style="height: 370px; width: 100%;"></div>
                        <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
                    </div>
                    <div class="col-md-12">
                        <div id="chartAnimalia" style="height: 370px; width: 100%;"></div>
                        <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
                    </div> -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end section -->

        

<?php include('footer.php') ?>