<?php include('header.php');
include('leftsidebar.php');
include('rightsidebar.php');

$novo_nome = md5(time()).'.jpg';
$_SESSION['foto_catalogacao'] = $novo_nome;
$_SESSION['id_catalogacao'] = $_GET['id'];



$catalogacao = new Catalogacao();
$catalogacao->setId($_GET['id']);
$array = $catalogacao->buscarCatalogacaoId();

while($catalogacao = mysqli_fetch_assoc($array)):
 ?>




    <div id="wrapper">

  <form action="editarfotocatalogacao.php" method="POST">

        <section class="section lb">
            <div class="container">
            	<div class="row">
            	<div class="col">


            	<center>
                  <label class="filebutton">

                  <div class="hovereffect">
                  <img   <?php echo ' src="images/imagem_catalogacao/'.$catalogacao["imagem_catalogacao"].'" ' ?>/>
                  <div class="overlay">
                  <h2 class="center-height">Alterar foto</h2>
                  </div>
                  </div>

                  <span><input type="file" name="upload_image" id="upload_image"></span>
                  </label>
                </center> 

            	</div>
              </div>
              
  <div id="uploadimageModal" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8 text-center">
                          <div id="image_demo" style="width:350px; margin-top:30px"></div>
                    </div>
                    <div class="col-md-4" style="padding-top:30px;">
                        <br />
                        <br />
                        <br/>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success crop_image" >Salvar</button>
            </div>
        </div>
    </div>
</div>

</form>



<form action="salvar_catalogacaoedit.php" method="POST">
<div class="mt-5">

<div class="row mt-2 mb-5 mx-3">
	<div class="col-6 mt-5">
    <label>Nome Popular</label>
		<input class="form-control" type="text" name="nome_popular" placeholder="Nome Popular" <?php echo 'value="'.$catalogacao["nome_popular"].'"' ?> required>
    <label class="mt-4">Filo</label>
		<input class="form-control " type="text" name="filo" placeholder="Filo" <?php echo 'value="'.$catalogacao["filo"].'"' ?> required>
    <label class="mt-4">Ordem</label>
		<input class="form-control" type="text" name="ordem" placeholder="Ordem" <?php echo 'value="'.$catalogacao["ordem"].'"' ?> required>
    <label class="mt-4">Familia</label>
		<input class="form-control" type="text" name="familia" placeholder="Família" <?php echo 'value="'.$catalogacao["familia"].'"' ?> required>
    <label class="mt-4">Genero</label>
		<input class="form-control" type="text" name="genero" placeholder="Gênero" <?php echo 'value="'.$catalogacao["genero"].'"' ?> required>
    <label class="mt-4">Espécie</label>
		<input class="form-control" type="text" name="especie" placeholder="Espécie" <?php echo 'value="'.$catalogacao["especie"].'"' ?> required>
    </div>
    <div class="col-6 mt-5">
      <div class="row mt-5">
        <div class="col">
        <select id="inputState" class="form-control" name="reino">
                  <option value='' disabled="disabled">Reino</option>
                <?php $linhas = mysqli_query($con, 'SELECT * from reino');
                while($reino = mysqli_fetch_assoc($linhas)): ?>
                  <option <?php if($catalogacao['id_reino'] == $reino['id']){echo 'selected="selected"';} echo 'value="'.$reino['id'].'"' ?>><?php echo $reino['descricao'] ?></option>
                <?php endwhile ?>
        </select>
		</div>
    </div>
        <label class="mt-4">Classe</label>
		<input class="form-control" type="text" name="classe" placeholder="Classe" <?php echo 'value="'.$catalogacao["classe"].'"' ?> required>
      <div class="form-group">

        <label class="mt-4">Descrição</label>
        <textarea class="form-control" rows="5" name="descricao" id="comment"  placeholder="Descrição"><?php if(isset($catalogacao['descricao'])){echo $catalogacao['descricao'];} ?></textarea>
        <label class="mt-4">Observação</label>
        <textarea class="form-control" rows="5" name="observacao" id="comment" placeholder="Observação"><?php if(isset($catalogacao['observacao'])){echo $catalogacao['observacao'];} ?></textarea>
      </div>

  </div>
  <div class="col mt-5">
    <center>
      <button type="submit" class="btn btn-primary mt-4 mr-3">Salvar alterações</button>
      <button type="button" class="btn btn-danger btn-lg mt-4" data-toggle="modal" data-target="#myModal">Excluir Catalogação</button>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <p>Tem certeza que deseja excluir essa catalogação? Essa é uma ação que não pode ser revertida</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Não</button>
        <?php echo '<a href="excluircatalogacao.php?id='.$_SESSION['id_catalogacao'].'" class="btn btn-success">' ?> Sim</a>
      </div>
    </div>

  </div>
</div>
  </center>
  </div>
</form>

<?php endwhile ?>

            </div><!-- end container -->
        </section><!-- end section -->



<script>  
$(document).ready(function(){

    $image_crop = $('#image_demo').croppie({
    enableExif: true,
    viewport: {
      width:300,
      height:200,
      type:'square' //circle
    },
    boundary:{
      width:300,
      height:300
    }
  });

  $('#upload_image').on('change', function(){
    var reader = new FileReader();
    reader.onload = function (event) {
      $image_crop.croppie('bind', {
        url: event.target.result
      }).then(function(){
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
    $('#uploadimageModal').modal('show');
  });

  $('.crop_image').click(function(event){
    $image_crop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function(response){
      $.ajax({
        url:"upload_catalogacao.php",
        type: "POST",
        data:{"image": response},
        success:function(data)
        {
          $('#uploadimageModal').modal('hide');
          $('#uploaded_image').html(data);
        }
      });
    })
  });

});  
</script>


<?php include('footer.php');
?>