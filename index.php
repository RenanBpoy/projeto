<?php include('header.php');

?>

<header class="vertical-left-header">
            <div class="vertical-left-header-wrapper">
                <nav class="nav-menu">
                    
                    <div class="logo">
                        <a href="index.php"><img src="images/logo.png" alt=""></a>
                    </div><!-- end logo -->

                    <div class="margin-block"></div>

                    <ul class="primary-menu">
                        
                                                               
                    <div class="margin-block"></div>

                    <div class="menu-search">
                        <form method="POST">
                            <?php
                            if(isset($_POST['email']) and isset($_POST['senha'])){
                                $email = $_POST['email'];
                                $senha = md5($_POST['senha']);
                                
                                $usuario = new Usuario();
                                $usuario->setEmail($email);
                                $usuario->setSenha($senha);

                                $result = $usuario->fazerLogin();
                                $user = mysqli_fetch_array($result);
                                if($user){
                                    $_SESSION['usuario'] = $user; 
                                    header("Location:/projeto/home.php");


                                }
                                else{
                                    echo '<div class="row p-3 ml-2"><p class="text-white">Usuário e/ou senha inválido(s)</p></div>';
                                }
                            }

                            ?>
                            <div class="form-group">
                                <input type="text" name="email" class="form-control" placeholder="Username ou Email">
                            </div>
                            <div class="form-group pt-3">
                                <input type="password" name="senha" class="form-control" placeholder="Senha">
                            </div>
                            <center><div class="form-group pt-3">
                                <button class="btn mt-2" type="submit">Enter</button>
                            </div></center>
                        </form>
                    </div><!-- end menu-search -->

                    <div class="margin-block"></div>

<!--                     <div class="menu-social">
                        <ul class="list-inline text-center">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                    </div> -->
                    <ul class="primary-menu">
                        <li><center><a href="register.php">Registre-se</a></center></li>
                    </ul>
                </nav><!-- end nav-menu -->
            </div><!-- end vertical-left-header-wrapper -->
        </header><!-- end header -->
    </div><!-- end menu-wrapper -->        

        

    <div id="wrapperlogin">

        <div id="home" class="video-section js-height-full">
            <div class="overlay"></div>
            <div class="home-text-wrapper relative container">
                <div class="home-message">
                    <img src="images/biglogo2.png" alt="">

                </div>
            </div>
        </div>



        
    </div><!-- end wrapper -->

    <!-- jQuery Files -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/carousel.js"></script>
    <script src="js/parallax.js"></script>
    <script src="js/rotate.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/masonry.js"></script>
    <script src="js/masonry-4-col.js"></script>
    <!-- VIDEO BG PLUGINS -->
    <script src="videos/libs/swfobject.js"></script> 
    <script src="videos/libs/modernizr.video.js"></script> 
    <script src="videos/libs/video_background.js"></script> 
    <script>
        jQuery(document).ready(function($) {
            var Video_back = new video_background($("#home"), { 
                "position": "absolute", //Follow page scroll
                "z-index": "-1",        //Behind everything
                "loop": true,           //Loop when it reaches the end
                "autoplay": true,       //Autoplay at start
                "muted": true,          //Muted at start
                "mp4":"videos/video.mp4" ,     //Path to video mp4 format
                "video_ratio": 1.7778,              // width/height -> If none provided sizing of the video is set to adjust
                "fallback_image": "images/dummy.png",   //Fallback image path
                "priority": "html5"             //Priority for html5 (if set to flash and tested locally will give a flash security error)
            });
        });
    </script>

</body>
</html>