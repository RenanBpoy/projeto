<?php include('header.php');
include('leftsidebar.php');
include('rightsidebar.php');

$novo_nome = md5(time()).'.jpg';
$_SESSION['foto_usuario'] = $novo_nome;


 ?>




    <div id="wrapper">

  <form action="editarfoto.php" method="POST">

        <section class="section lb">
            <div class="container">
            	<div class="row">
            		<div class="col-4">



                  <label class="filebutton circle-image">

                  <div class="hovereffect">
                  <img width="200" height="200" class="circle-image picture"  <?php echo ' src="images/imagem_usuario/'.$_SESSION['usuario']['imagem'].'" ' ?>/>
                  <div class="overlay">
                  <h2 class="center-height">Alterar foto</h2>
                  </div>
                  </div>

                  <span><input type="file" name="upload_image" id="upload_image"></span>
                  </label>
                  
            		</div>
            		<div class="col-4 mt-5">
            			<h1 class="h0"><?php echo $_SESSION['usuario']['nome'] ?></h1>
            			<h1>@<?php echo $_SESSION['usuario']['username'] ?></h1>
            		</div>
            	</div>
      <div id="uploadimageModal" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8 text-center">
                          <div id="image_demo" style="width:350px; margin-top:30px"></div>
                    </div>
                    <div class="col-md-4" style="padding-top:30px;">
                        <br />
                        <br />
                        <br/>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success crop_image" >Salvar</button>
            </div>
        </div>
    </div>
</div>

</form>



<form action="salvar_perfil.php" method="POST">
<div class="mt-5">

  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputEmail4">Email</label>
      <input name="email" type="email" class="form-control" id="inputEmail4" placeholder="Email" required <?php echo 'value="'.$_SESSION["usuario"]["email"].'"' ?>>
    </div>
    <div class="form-group col-md-6">
      <label for="inputCity">Username</label>
      <input name="username" type="text" class="form-control" id="inputCity" <?php echo 'value="'.$_SESSION["usuario"]["username"].'"' ?>>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputEmail4">Nome</label>
      <input name="nome" type="text" class="form-control" id="inputEmail4" placeholder="Nome" required <?php echo 'value="'.$_SESSION["usuario"]["nome"].'"' ?>>
    </div>
    <div class="form-group col-md-6">
      <label for="inputPassword4">Sobrenome</label>
      <input name="sobrenome" type="text" class="form-control" id="inputPassword4" placeholder="Sobrenome" required <?php echo 'value="'.$_SESSION["usuario"]["sobrenome"].'"' ?>>
    </div>
  </div>
  <div class="form-group">
    <label for="inputAddress2">Data de Nascimento</label>
    <input name="data_nascimento" type="date" class="form-control" id="inputAddress2" <?php echo 'value="'.$_SESSION["usuario"]["data_nascimento"].'"' ?> >
  </div>
  <div class="form-row">

    <div class="form-group col-md-4">
      <label for="inputState">Sexo</label>
      <select id="inputState" class="form-control" name="id_sexo">
        <option value="" disabled="disabled">Sexo</option>
        <?php $linhas = mysqli_query($con, 'SELECT * from sexo');
        while($sexo = mysqli_fetch_assoc($linhas)): ?>

          <option <?php if($sexo['id'] == $_SESSION['usuario']['id_sexo']){echo 'selected="selected"';} echo 'value="'.$sexo['id'].'"' ?>><?php echo $sexo['descricao'] ?></option>
        <?php endwhile ?>
      </select>
    </div>
    <div class="form-group col-md-4">
      <label for="inputZip">CRBio</label>
      <input name="crbio" type="text" class="form-control" id="inputZip" <?php echo 'value="'.$_SESSION["usuario"]["crbio"].'"' ?>>
    </div>

<div class="form-group col-md-4">

<?php

  if($_SESSION['senha'] == 0){?>
  <div class="row ml-3">
  <label for="inputZip">Senha</label>
  </div>

  <?php }

  else if($_SESSION['senha'] == 1){?>
  <div class="row ml-3">
  <label for="inputZip">As senhas não conferem</label>
  </div>

  <?php $_SESSION['senha'] = 0;}
  else if($_SESSION['senha'] == 2){?>
  <div class="row ml-3">
  <label for="inputZip">Senha atual incorreta</label>
  </div>

  <?php $_SESSION['senha'] = 0; }

?>

<div class="btn-group ml-3">
  <button class="btn btn-light btn-sm  btn-height" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <div class="ty">Alterar Senha<div>
  </button>
  <div class="dropdown-menu">
        <input name="senha_atual" type="password" class="form-control mt-1" placeholder="Senha atual" >
        <input name="nova_senha" type="password" class="form-control mt-1" placeholder="Nova senha" >
        <input name="nova_senha2" type="password" class="form-control mt-1"  placeholder="Confirmar nova senha" >
  </div>
</div>
</div>


  </div>

  <center>
    <button type="submit" class="btn btn-primary mt-4 mr-3">Salvar alterações</button>
    <button type="button" class="btn btn-danger btn-lg mt-4" data-toggle="modal" data-target="#myModal">Excluir Conta</button>
  </center>

  <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <p>Tem certeza que deseja excluir sua conta? Essa é uma ação que não pode ser revertida</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Não</button>
        <a href="excluirperfil.php" class="btn btn-success">Sim</a>
      </div>
    </div>

  </div>
</div>

</div>
</form>

            </div><!-- end container -->
        </section><!-- end section -->



<script>  
$(document).ready(function(){

    $image_crop = $('#image_demo').croppie({
    enableExif: true,
    viewport: {
      width:200,
      height:200,
      type:'square' //circle
    },
    boundary:{
      width:300,
      height:300
    }
  });

  $('#upload_image').on('change', function(){
    var reader = new FileReader();
    reader.onload = function (event) {
      $image_crop.croppie('bind', {
        url: event.target.result
      }).then(function(){
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
    $('#uploadimageModal').modal('show');
  });

  $('.crop_image').click(function(event){
    $image_crop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function(response){
      $.ajax({
        url:"upload_profile.php",
        type: "POST",
        data:{"image": response},
        success:function(data)
        {
          $('#uploadimageModal').modal('hide');
          $('#uploaded_image').html(data);
        }
      });
    })
  });

});  
</script>


<?php include('footer.php');
?>