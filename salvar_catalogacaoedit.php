<?php 
include('header.php');

	$id = $_SESSION['id_catalogacao'];
	$nome_popular = $_POST['nome_popular'];
	$filo = $_POST['filo'];
	$classe = $_POST['classe'];
	$ordem = $_POST['ordem'];
	$familia = $_POST['familia'];
	$genero = $_POST['genero'];
	$especie = $_POST['especie'];
	$descricao = '';
	$observacao = '';

	if (isset($_POST['descricao'])){
		$descricao = $_POST['descricao'];
	};

	if (isset($_POST['observacao'])){
		$observacao = $_POST['observacao'];
	};

	$id_usuario = $_SESSION['usuario']['id'];
	$id_reino = $_POST['reino'];

	$catalogacao = new Catalogacao();
	$catalogacao->setId($id);
	$catalogacao->setNomePopular($nome_popular);
	$catalogacao->setFilo($filo);
	$catalogacao->setClasse($classe);
	$catalogacao->setOrdem($ordem);
	$catalogacao->setFamilia($familia);
	$catalogacao->setGenero($genero);
	$catalogacao->setEspecie($especie);
	$catalogacao->setDescricao($descricao);
	$catalogacao->setObservacao($observacao);
	$catalogacao->setIdUsuario($id_usuario);
	$catalogacao->setIdReino($id_reino);

	echo $catalogacao->alterarCadastro();


if($catalogacao->alterarCadastro())

    header("Location:/projeto/minhas_catalogacoes.php");
    exit();   


?>

<?php include('footer.php') ?>