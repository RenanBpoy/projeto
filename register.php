<?php include('header.php');

 ?>

<header class="vertical-left-header">
            <div class="vertical-left-header-wrapper">
                <nav class="nav-menu">
                    <div class="logo">
                        <a href="index.php"><img src="images/logo.png" alt=""></a>
                    </div><!-- end logo -->


                    <ul class="primary-menu">
                                                               

                    <div class="menu-search">
                        <form action="salvar.php" method="POST">
                            <div class="form-group">
                                <input name="nome" type="text" class="form-control" placeholder="Nome" required>
                            </div>
                            <div class="form-group mt-3">
                                <input name="sobrenome" type="text" class="form-control" placeholder="Sobrenome" required>
                            </div>
                            <div class="form-group mt-3">
                                <input name="data_nascimento" type="date" class="form-control" placeholder="Data de Nascimento" required>
                            </div>
                            <div class="form-group mt-3">
                                <input name="username" type="text" class="form-control" placeholder="Username" required>
                            </div>
                            <div class="form-group mt-3">
                                <input name="crbio" type="text" class="form-control" placeholder="CRBio">
                            </div>
                            <div class="form-group mt-3">
                                <input name="email" type="email" class="form-control" placeholder="E-mail" required>
                            </div>
                            <div class="form-group mt-3">
                                <input name="senha" type="password" class="form-control" placeholder="Senha" required>
                            </div>
                            <div class="form-group mt-3">
                                <input name="confirmarsenha" type="password" class="form-control" placeholder="Confirmar Senha" required>
                            </div>
                            <div class="form-group mt-3">
                            <select class="btn  btn-block dropdown-toggle text-default" name="id_sexo" required>
                                <option value="" disabled="disabled" selected="selected">Sexo</option>
                            <?php $linhas = mysqli_query($con, 'SELECT * from sexo');
                            while($sexo = mysqli_fetch_assoc($linhas)): ?>
                              <option <?php echo 'value="'.$sexo['id'].'"' ?>><?php echo $sexo['descricao'] ?></option>
                            <?php endwhile ?>
                            </select>
                            </div>
                            <div class="mt-8">
                                <input type="submit" class="form-control">
                            </div>
                        </form>
                    </div><!-- end menu-search -->

                    <div class="margin-block"></div>

                </nav><!-- end nav-menu -->
            </div><!-- end vertical-left-header-wrapper -->
        </header><!-- end header -->
    </div><!-- end menu-wrapper -->        

        

    <div id="wrapperlogin">

        <div id="home" class="video-section js-height-full">
            <div class="overlay"></div>
            <div class="home-text-wrapper relative container">
                <div class="home-message">
                    <img src="images/biglogo2.png" alt="">

                </div>
            </div>
        </div>



        
    </div><!-- end wrapper -->

    <!-- jQuery Files -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/carousel.js"></script>
    <script src="js/parallax.js"></script>
    <script src="js/rotate.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/masonry.js"></script>
    <script src="js/masonry-4-col.js"></script>
    <!-- VIDEO BG PLUGINS -->
    <script src="videos/libs/swfobject.js"></script> 
    <script src="videos/libs/modernizr.video.js"></script> 
    <script src="videos/libs/video_background.js"></script> 
    <script>
        jQuery(document).ready(function($) {
            var Video_back = new video_background($("#home"), { 
                "position": "absolute", //Follow page scroll
                "z-index": "-1",        //Behind everything
                "loop": true,           //Loop when it reaches the end
                "autoplay": true,       //Autoplay at start
                "muted": true,          //Muted at start
                "mp4":"videos/video.mp4" ,     //Path to video mp4 format
                "video_ratio": 1.7778,              // width/height -> If none provided sizing of the video is set to adjust
                "fallback_image": "images/dummy.png",   //Fallback image path
                "priority": "html5"             //Priority for html5 (if set to flash and tested locally will give a flash security error)
            });
        });
    </script>

</body>
</html>