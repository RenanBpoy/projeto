<?php 
session_start();
ob_start();

require_once "class.php";

if (!isset($_SESSION['foto'])){
  $_SESSION['foto'] = 0;
}
if (!isset($_SESSION['foto_usuario'])){
  $_SESSION['foto_usuario'] = 0;
}

if(!isset($_SESSION['senha'])){
    $_SESSION['senha'] = 0;
}

$con = mysqli_connect('127.0.0.1', 'root', 'root', 'biocense');
$_SESSION['con'] = $con;

if(isset($_SESSION['usuario'])){
include('medalha.php');}
?>



<!doctype html>
<!--[if IE 9]> <html class="no-js ie9 fixed-layout" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js " lang="en"> <!--<![endif]-->
<head>

    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    
    <!-- Site Meta -->
    <title>Biocense</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    
    <!-- Site Icons -->
<link rel="icon" href="images/apple-touch-icon.png">

	<!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet"> 

	<!-- Custom & Default Styles -->

	<link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/carousel.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="bootstrap.min.css" />
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="style2.css">
    <link rel="stylesheet" href="style3.css">
    <link rel="stylesheet" href="style4.css">
    <link rel="stylesheet" href="style5.css">
    <link rel="stylesheet" href="stylefonts.css">
    <link rel="stylesheet" href="usercard.css">

    <script>
        window.onload = function () {
         
        var chart = new CanvasJS.Chart("chartGeral", {
            animationEnabled: true,
            theme: "light2",
            title:{
                text: "Geral"
            },

            data: [{
                type: "column",

                dataPoints: <?php echo json_encode($dataGeral, JSON_NUMERIC_CHECK); ?>
            }]
        });
        chart.render();

        var chart = new CanvasJS.Chart("chartPlantae", {
            animationEnabled: true,
            theme: "light2",
            title:{
                text: "Plantae"
            },

            data: [{
                type: "column",

                dataPoints: <?php echo json_encode($dataPlantae, JSON_NUMERIC_CHECK); ?>
            }]
        });
        chart.render();

        var chart = new CanvasJS.Chart("chartAnimal", {
            animationEnabled: true,
            theme: "light2",
            title:{
                text: "Animalia"
            },

            data: [{
                type: "column",

                dataPoints: <?php echo json_encode($dataAnimalia, JSON_NUMERIC_CHECK); ?>
            }]
        });
        chart.render();
         

         
        }
    </script>
<script>


</script>



</head>
<body class="left-menu">  
    
    <div class="menu-wrapper">