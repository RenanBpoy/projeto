<?php
include('header.php');
include('leftsidebar.php');
include('rightsidebar.php');


?>
        


    <div id="wrapper">


        <section class="section lb">
            <div class="container">
                <div class="section-title text-center">
                    <h3>Meus prêmios</h3>
                    <hr>
                </div><!-- end title -->
                <div class="row">
                    <?php

                    $sql = "SELECT * FROM premiacao p JOIN medalha m on m.id = p.id_medalha WHERE p.id_usuario = ".$_SESSION['usuario']['id'];
                    $sql_query = mysqli_query($_SESSION['con'], $sql);
                    if (mysqli_num_rows($sql_query)==0) { ?>
                      <div class="col"><center><h4>Ops... parece que você não possui premiações</h4></center></div>
                    <?php }
                    else{
                    while($medalhas = mysqli_fetch_assoc($sql_query)):?>
                    <div class="col-3">
                        <img <?php echo 'src="images/imagem_medalha/'.$medalhas['imagem_medalha'].'"' ?> class="img-fluid">
                    </div>
                    <?php endwhile; } ?>

                </div>

            </div><!-- end container -->
        </section><!-- end section -->

        

<?php include('footer.php') ?>