<header class="vertical-right-header">
    <div class="vertical-right-header-wrapper">
        <center><h3 class="text-default mt-5">Ranking <i class="fa fa-trophy"></i> </h3></center>
        <ul class="primary-menu">
            <div class="mt-negative-1">
            <?php

            $sqlrank = 'SELECT u.id as id_usuario, c.*, u.*, COUNT(c.nome_popular) "catalogacoes" FROM usuario u JOIN catalogacao c on c.id_usuario = u.id GROUP BY u.nome ORDER BY COUNT(c.nome_popular) DESC LIMIT 3';

            $listrank = mysqli_query($_SESSION['con'], $sqlrank);
            $count = 0;
            while($ranking = mysqli_fetch_array($listrank)):
            $count++; ?>
            <li class="child-menu"><a <?php echo 'href="userprofile.php?id='.$ranking["id_usuario"].'"' ?>><?php echo $count . 'º - ' ?><img class="circle-image mr-3" <?php echo 'src="images/imagem_usuario/'.$ranking['imagem'].'"' ?> width="30px" height="30px"><?php echo $ranking['nome'].' - '.$ranking['catalogacoes'] ?></a>
                <div class="sub-menu-right-wrapper">
                    <ul class="sub-menu-right">
                            <div class="row mt-4">
                                <div class="col-4 mt-3">
                                    <a <?php echo 'href="userprofile.php?id='.$ranking["id_usuario"].'"' ?>><img class="img-fluid circle-image zoom" <?php echo 'src="images/imagem_usuario/'.$ranking['imagem'].'"' ?> alt=""></a>
                                </div>
                                <div class="col-8 mt-2 ml-negative">
                                    <h3 class="mt-2 text-default"><?php echo $ranking['nome'] ?></h3>
                                    <h5 class="mt-1 text-default">@<?php echo $ranking['username'] ?></h5>
                                </div>
                            </div>
                            <center><h4 class="text-default mt-5">Catalogações recentes</h4></center>
                            <ul class="mt-5">

                            <?php
                                $id_usuario_catalogacao = $ranking['id_usuario'];
                                $sql = 'SELECT * FROM catalogacao WHERE id_usuario='.$id_usuario_catalogacao.' ORDER BY data_hora_catalogacao DESC LIMIT 3';
                                $listaposts = mysqli_query($_SESSION['con'], $sql);
                                while($posts = mysqli_fetch_array($listaposts)):?>

                                <li> <a <?php echo 'href="catalogacao_detail.php?id='.$posts['id'].'"' ?>>                                   
                                    <img <?php echo ' src="images/imagem_catalogacao/'.$posts['imagem_catalogacao'].'" ' ?> alt="" class="img-responsive img-fluid"></a>
                                    <div class="magnifier">
                                        <a class="golink" href="#">
                                           <i class="fa fa-link"></i>
                                        </a>
                                    </div><!-- end magnifier -->
                                    <center><a href="#"></a></center>
                                </li>

                            <?php endwhile ?>
                            </ul>
                            <ul class="mt-4">
                                  <?php 
                                        if ($id_usuario_catalogacao == $_SESSION['usuario']['id']){ ?>
                                        <li><a href="editarperfil.php"><center>Editar perfil</center></a></li>
                                        <?php 
                                        }
                                        else{ 
                                          $seguir = new Seguir();
                                          $seguir->setIdUsuario1($_SESSION['usuario']['id']);
                                          $seguir->setIdUsuario2($id_usuario_catalogacao);
                                          $array_amigos = $seguir->consultarSeguir();
                                          $amigos = mysqli_fetch_array($array_amigos);
                                          if($amigos){?>

                                            <li><a <?php echo 'href="deixarseguir.php?id_usuario2='.$id_usuario_catalogacao.'"' ?>>Deixar de seguir</a></li>
                                          

                                        <?php
                                          }
                                          else{
                                        ?>
                                          <li><a <?php echo 'href="seguir.php?id_usuario2='.$id_usuario_catalogacao.'"' ?>><center>Seguir</center></a></li>

                                          <?php
                                          }
                                        }
                                    ?>

                            </ul>
                    </ul>
                </div>
            </li>
            <?php endwhile ?>
            </div>
        </ul>
        <center><h3 class="text-default mt-5">Amigos <i class="fa fa-user"></i> </h3></center>


        <ul class="primary-menu">


        <div class="mt-negative-2">

            <div class="menu-search">
                <form action="searchuser.php">
                    <div class="form-group">
                        <input name="pesquisar_usuario" type="text" class="form-control" placeholder="Pesquisar usuário">
                        <button class="btn btn-primary mt-2"><i class="fa fa-search"></i></button>
                    </div>
                </form>
            </div><!-- end menu-search -->

        </div>
        <div class="mt-5">
            <?php 

          $seguir = new Seguir();
          $seguir->setIdUsuario1($_SESSION['usuario']['id']);
          $array = $seguir->consultarAmizade();
          while($amigos = mysqli_fetch_array($array)): ?>

            <li class="child-menu"><a <?php echo 'href="userprofile.php?id='.$amigos["id"].'"' ?>><img class="circle-image mr-3" <?php echo 'src="images/imagem_usuario/'.$amigos['imagem'].'"' ?> width="30px" height="30px"><?php echo $amigos['nome'] ?></a>
                <div class="sub-menu-right-wrapper">
                    <ul class="sub-menu-right">
                            <div class="row mt-4">
                                <div class="col-4 mt-3">
                                    <a <?php echo 'href="userprofile.php?id='.$amigos["id"].'"' ?>><img class="img-fluid circle-image zoom" <?php echo 'src="images/imagem_usuario/'.$amigos['imagem'].'"' ?> alt=""></a>
                                </div>
                                <div class="col-8 mt-2 ml-negative">
                                    <h3 class="mt-2 text-default"><?php echo $amigos['nome'] ?></h3>
                                    <h5 class="mt-1 text-default"><?php echo $amigos['username'] ?></h5>
                                </div>
                            </div>
                            <center><h4 class="text-default mt-5">Catalogações recentes</h4></center>
                            <ul class="mt-5">
                                <?php

                                $id_usuario_catalogacao = $amigos['id_usuario2'];
                                $sql = 'SELECT * FROM catalogacao WHERE id_usuario='.$id_usuario_catalogacao.' ORDER BY data_hora_catalogacao DESC LIMIT 3';
                                $listaposts = mysqli_query($_SESSION['con'], $sql);
                                while($posts = mysqli_fetch_array($listaposts)):?>
                                <li> <a <?php echo 'href="catalogacao_detail.php?id='.$posts['id'].'"' ?>>                                   
                                    <img <?php echo ' src="images/imagem_catalogacao/'.$posts['imagem_catalogacao'].'" ' ?> alt="" class="img-responsive img-fluid"></a>
                                    <div class="magnifier">
                                        <a class="golink" href="#">
                                           <i class="fa fa-link"></i>
                                        </a>
                                    </div><!-- end magnifier -->
                                    <center ><a href="#"></a></center>
                                </li>
                                <?php endwhile ?>
                            </ul>
                            <ul class="mt-4">
                                  <?php 
                                        if ($id_usuario_catalogacao == $_SESSION['usuario']['id']){ ?>
                                        <li><a href="editarperfil.php">Editar perfil</a></li>
                                        <?php 
                                        }
                                        else{ 
                                          $seguir = new Seguir();
                                          $seguir->setIdUsuario1($_SESSION['usuario']['id']);
                                          $seguir->setIdUsuario2($id_usuario_catalogacao);
                                          $array_amigos = $seguir->consultarSeguir();
                                          $amigos = mysqli_fetch_array($array_amigos);
                                          if($amigos){?>

                                            <li><a <?php echo 'href="deixarseguir.php?id_usuario2='.$id_usuario_catalogacao.'"' ?>>Deixar de seguir</a></li>
                                          

                                        <?php
                                          }
                                          else{
                                        ?>
                                          <li><a <?php echo 'href="seguir.php?id_usuario2='.$id_usuario_catalogacao.'"' ?>>Seguir</a></li>

                                          <?php
                                          }
                                        }
                                    ?>
                            </ul>
                    </ul>
                </div>
            </li>

            <?php

            endwhile;

            ?>
            
            </div>
        </ul>
    </div>
</header>
    </div><!-- end menu-wrapper -->